package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jdistlib.Exponential;
import jdistlib.LogNormal;
import jdistlib.Uniform;
import jdistlib.evd.GeneralizedPareto;
import jdistlib.generic.GenericDistribution;

import statistics.InputData;

public class DistributionSetterPanel extends JPanel {
	private JTextField param1TextField;
	private JTextField param2TextField;
	private JTextField param3TextField; 
	private JComboBox<String> comboBox;
	
	private int columnOfTextField = 3;	
	private static String[] distributionOptions = {"Eksponencjalny","Log-Normalny","Jednostajny", "Uog�lnionyPareto"};
	
	public DistributionSetterPanel() {
		initialize();
		
	}

	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);	
		
		comboBox = new JComboBox<String>(distributionOptions);
		setComboboxListener(comboBox);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.anchor = GridBagConstraints.EAST;
		//gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		//gbc_comboBox.weightx = 3;
		this.add(comboBox, gbc_comboBox);
		
		InputData.comboBoxDistributionValues.add(comboBox);
		
		param1TextField = new JFormattedTextField(getNumberFormat());
		GridBagConstraints gbc_textField = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		//gbc_textField.weightx = 2;
		this.add(param1TextField, gbc_textField);
		param1TextField.setColumns(columnOfTextField);
		param1TextField.setText("1,0");
		
		InputData.textFieldsDistributionValues.add(param1TextField);
		
		param2TextField = new JFormattedTextField(getNumberFormat());	
		GridBagConstraints par1gbc = new GridBagConstraints();
		//par1gbc.insets = new Insets(0, 0, 5, 5);
		par1gbc.fill = GridBagConstraints.HORIZONTAL;
		par1gbc.gridx = 2;
		par1gbc.gridy = 0;
		//par1gbc.weightx = 2;
		this.add(param2TextField, par1gbc);
		param2TextField.setColumns(columnOfTextField);
		param2TextField.setText("1,0");
		
		InputData.textFieldsDistributionValues.add(param2TextField);
		
		param3TextField = new JFormattedTextField(getNumberFormat());		
		GridBagConstraints par3gbc = new GridBagConstraints();
		//par1gbc.insets = new Insets(0, 0, 5, 5);
		par3gbc.fill = GridBagConstraints.HORIZONTAL;
		par3gbc.gridx = 3;
		par3gbc.gridy = 0;
		//par1gbc.weightx = 2;
		this.add(param3TextField, par3gbc);
		param3TextField.setColumns(columnOfTextField);
		param3TextField.setText("1,0");
		
		InputData.textFieldsDistributionValues.add(param3TextField);
		
		//Important to Deactive addictional fields
		comboBox.setSelectedIndex(0);
	}
	
	private void setComboboxListener(JComboBox<String> combobox){
		ActionListener  itemListener = new ActionListener () {
	        public void actionPerformed(ActionEvent  itemEvent) {
	    		switch((String)comboBox.getSelectedItem()){
					case "Eksponencjalny":	param2TextField.setEnabled(false); param3TextField.setEnabled(false); break;
					case "Log-Normalny": 	param2TextField.setEnabled(true);  param3TextField.setEnabled(false); break;	
					case "Jednostajny":		param2TextField.setEnabled(true);  param3TextField.setEnabled(false); break;
					case "Uog�lnionyPareto":param2TextField.setEnabled(true);  param3TextField.setEnabled(true);  break;
	    		}
	        	
//	          int state = itemEvent.getStateChange();
//	          System.out.println((state == ItemEvent.SELECTED) ? "Selected" : "Deselected");
//	          System.out.println("Item: " + itemEvent.getItem());
//	          ItemSelectable is = itemEvent.getItemSelectable();
//	          System.out.println(", Selected: " + selectedString(is));
	        }
	      };
	      combobox.addActionListener(itemListener);
	}

	public GenericDistribution getDistributionGenerator(){
		switch((String)comboBox.getSelectedItem()){
			case "Eksponencjalny":	return new Exponential(getNumberParam(param1TextField));
			case "Log-Normalny": 	return new LogNormal(getNumberParam(param1TextField),getNumberParam(param2TextField));
			case "Jednostajny":		return new Uniform(getNumberParam(param1TextField),getNumberParam(param2TextField));
			case "Uog�lnionyPareto":return new GeneralizedPareto(getNumberParam(param1TextField),getNumberParam(param2TextField),getNumberParam(param3TextField));
			default : return null;
		}
	}
	
	private double getNumberParam(JTextField field){
		if (field.getText() == null || field.getText().equals(""))
			return 0.0;
		else
			return Double.parseDouble(field.getText().replace(',', '.')); 
	}
	
	private NumberFormat getNumberFormat(){
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setGroupingUsed(false);
		format.setMinimumFractionDigits(1);
		format.setMinimumIntegerDigits(1);
		return format;
	}
}