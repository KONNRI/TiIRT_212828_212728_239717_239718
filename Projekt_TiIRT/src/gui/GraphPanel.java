package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import gui.charts.HistogramSaver;

public class GraphPanel extends JPanel
{
	private XYSeriesCollection dataset;
	private XYSeries throughputUsing, filteredThroughputUsing;
	private JCheckBox checkBox;
	private JSpinner spinner;
	private JButton button;
	public static int TIME_TO_SHOW = Integer.MAX_VALUE;
	private static String CHART_TITLE = "Wykorzystanie przepustowo�ci w�z�a w czasie";
	private JFreeChart lineChart;
	
	public GraphPanel()
	{
		this.dataset = new XYSeriesCollection();
		this.throughputUsing = new XYSeries("Zape�nienie w�z�a");
		this.filteredThroughputUsing = new XYSeries("Zape�nienie w�z�a");
		dataset.addSeries(filteredThroughputUsing);
		lineChart = ChartFactory.createXYLineChart(
				CHART_TITLE, 
				"Czas",
				"% zape�niania",
				dataset,
				PlotOrientation.VERTICAL,
				true, true, false);
		
		//CategoryPlot catPlot = lineChart.getCategoryPlot();
		
		//CategoryAxis domain = (CategoryAxis) catPlot.getDomainAxis();
		//domain.setTick
		//range.setVerticalTickLabels(true);
		
		ChartPanel chart = new ChartPanel(lineChart);
		chart.setPreferredSize(new Dimension(500,300));
		add(chart);
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setPreferredSize(new Dimension(100, 100));
		JPanel panelA = new JPanel();
		panelA.setAlignmentX(LEFT_ALIGNMENT);
		//panel.setPreferredSize(new Dimension(100,50));
		checkBox = new JCheckBox("Wybierz okres");
		panelA.add(checkBox);
		panel.add(panelA);
		
		SpinnerNumberModel spinnerModel = new SpinnerNumberModel(5, 0, 9999, 1); 
		spinner = new JSpinner(spinnerModel);
		spinner.setPreferredSize(new Dimension(50, 20));
		spinner.setVisible(false);
		JPanel panelB = new JPanel();
		panelB.setAlignmentX(LEFT_ALIGNMENT);
		panelB.add(spinner);
		panel.add(panelB);
		
		button = new JButton("OK");
		JPanel panelC = new JPanel();
		panelC.setAlignmentX(LEFT_ALIGNMENT);
		panelC.add(button);
		button.setVisible(false);
		panel.add(panelC);
		
		add(panel);
		
		setListenners();
	}
	
	public XYSeriesCollection setDataSet(ArrayList<Integer> throughput, ArrayList<BigDecimal> time)
	{
		dataset = null;
		dataset = new XYSeriesCollection();
		for(int i = 0; i < throughput.size(); i++)
		{
			throughputUsing.add(throughput.get(i), new Double(time.get(i).doubleValue()));
			filteredThroughputUsing.add(throughput.get(i), new Double(time.get(i).doubleValue()));
		}
		return dataset;
	}
	
	public XYSeriesCollection getDataset()
	{
		return dataset;
	}
	
	public void updateChart(BigDecimal throughput, BigDecimal time)
	{
		throughputUsing.add(new Double(time.doubleValue()), new Double(throughput.doubleValue()));
		filteredThroughputUsing.add(new Double(time.doubleValue()), new Double(throughput.doubleValue()));
		if(getTimeValue() != Integer.MAX_VALUE)
		{
			for(int i = 0; i < filteredThroughputUsing.getItemCount(); i++)
			{
				if(filteredThroughputUsing.getMaxX() - filteredThroughputUsing.getDataItem(i).getXValue() > getTimeValue())
					filteredThroughputUsing.remove(i);
			}
		
		}
	}
	
	public void clearDataSet() 
	{
		throughputUsing.clear();
		filteredThroughputUsing.clear();
	}
	
	public void setListenners()
	{
		checkBox.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(checkBox.isSelected())
				{
					spinner.setVisible(true);
					button.setVisible(true);
				}
				else
				{
					spinner.setVisible(false);
					button.setVisible(false);
				}
			}
		});
		
		button.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() 
				{

					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						TIME_TO_SHOW = (Integer) spinner.getValue();
						updateAfterClick();
					}
					
				});
			}
		});
	}
	
	public synchronized void updateAfterClick()
	{
		filteredThroughputUsing.clear();
		for(int i = 0; i < throughputUsing.getItemCount(); i++)
		{
			filteredThroughputUsing.add(throughputUsing.getDataItem(i));
		}
	}
	synchronized int getTimeValue()
	{
		return TIME_TO_SHOW;
	}
	
	public void saveAsImg(){
		HistogramSaver.save(lineChart, CHART_TITLE+".PNG", 600, 300);
	}
}
