package gui;

import java.awt.Color;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import algoritms.acmissioncontrol.AccessControlAlgorith;

import statistics.InputData;

public class AlgorithmPanel extends JPanel
{
	private JComboBox chosenQueuingAlgorithm, chosenAccessControlAlgorithm, chosenLoadParameters;
	private GUI gui;
	private int columnOfTextField = 3;
	private JTextField minth1TextField, minth2TextField, minth3TextField, minth4TextField, minth5TextField;
	private JTextField maxth1TextField, maxth2TextField, maxth3TextField, maxth4TextField, maxth5TextField;
	private static String[] AccessControlOptions = {"Wybierz","RED","WRED", "RIO"};
	
	public AlgorithmPanel(GUI gui)
	{
		this.gui = gui;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);
		
		JLabel queuingLabel = new JLabel("Algorytm kolejkowania:");
		GridBagConstraints gbc_queuingLabel = new GridBagConstraints();
		gbc_queuingLabel.insets = new Insets(0, 0, 5, 5);
		gbc_queuingLabel.gridx = 0;
		gbc_queuingLabel.gridy = 0;
		this.add(queuingLabel, gbc_queuingLabel);
		
		chosenQueuingAlgorithm = new JComboBox(); 			// TODO: sparametryzowac
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		//gbc_comboBox.weightx = 3;
		//chosenQueuingAlgorithm.addItem("Wybierz");
		chosenQueuingAlgorithm.addItem("FIFO");
		chosenQueuingAlgorithm.addItem("SJF");
		chosenQueuingAlgorithm.addItem("PQ");
		chosenQueuingAlgorithm.addItem("WFQ");
		chosenQueuingAlgorithm.addItem("HTB");
		this.add(chosenQueuingAlgorithm, gbc_comboBox);
		
		JLabel accessAlgorithmLabel = new JLabel("Algorytm kontroli dost�pu:");
		GridBagConstraints gbc_accessAlgorithmLabel = new GridBagConstraints();
		gbc_accessAlgorithmLabel.anchor = GridBagConstraints.WEST;
		gbc_accessAlgorithmLabel.insets = new Insets(0, 15, 5, 5);
		gbc_accessAlgorithmLabel.gridx = 0;
		gbc_accessAlgorithmLabel.gridy = 1;
		this.add(accessAlgorithmLabel, gbc_accessAlgorithmLabel);
		
		chosenAccessControlAlgorithm = new JComboBox<String>(AccessControlOptions); 			
		GridBagConstraints gbc_comboBox1 = new GridBagConstraints();
		gbc_comboBox1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox1.gridx = 1;
		gbc_comboBox1.gridy = 1;
		//gbc_comboBox.weightx = 3;
		this.add(chosenAccessControlAlgorithm, gbc_comboBox1);
		setComboboxListener(chosenAccessControlAlgorithm);
		
		JLabel loadParameters = new JLabel("Wczytaj zestaw parametr�w:");
		GridBagConstraints gbc_loadParameters = new GridBagConstraints();
		gbc_loadParameters.anchor = GridBagConstraints.WEST;
		gbc_loadParameters.insets = new Insets(0, 15, 5, 5);
		gbc_loadParameters.gridx = 0;
		gbc_loadParameters.gridy = 2;
		this.add(loadParameters, gbc_loadParameters);
		
		chosenLoadParameters = new JComboBox(); 
		GridBagConstraints gbc_chosenLoadParameters = new GridBagConstraints();
		gbc_chosenLoadParameters.insets = new Insets(0, 0, 5, 5);
		gbc_chosenLoadParameters.gridx = 1;
		gbc_chosenLoadParameters.gridy = 2;
		//gbc_comboBox.weightx = 3;
		chosenLoadParameters.addItem("Wybierz");
		
		JLabel maxth1= new JLabel("maxth1:");
		GridBagConstraints gbc_maxth1 = new GridBagConstraints();
		gbc_maxth1.anchor = GridBagConstraints.WEST;
		gbc_maxth1.insets = new Insets(0, 15, 5, 5);
		gbc_maxth1.gridx = 0;
		gbc_maxth1.gridy = 4;
		this.add(maxth1, gbc_maxth1);
		
		maxth1TextField = new JFormattedTextField();
		GridBagConstraints gbc_maxth1TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_maxth1TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxth1TF.gridx = 1;
		gbc_maxth1TF.gridy = 4;
		//gbc_textField.weightx = 2;
		this.add(maxth1TextField, gbc_maxth1TF);
		maxth1TextField.setColumns(columnOfTextField);
		maxth1TextField.setText("50");
		maxth1TextField.setEnabled(false);
		AccessControlAlgorith.maxthValues.add(maxth1TextField);
		
		JLabel minth1= new JLabel("minth1:");
		GridBagConstraints gbc_minth1 = new GridBagConstraints();
		gbc_minth1.anchor = GridBagConstraints.WEST;
		gbc_minth1.insets = new Insets(0, 15, 5, 5);
		gbc_minth1.gridx = 0;
		gbc_minth1.gridy = 5;
		this.add(minth1, gbc_minth1);
		
		minth1TextField = new JFormattedTextField();
		GridBagConstraints gbc_minth1TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_minth1TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minth1TF.gridx = 1;
		gbc_minth1TF.gridy = 5;
		//gbc_textField.weightx = 2;
		this.add(minth1TextField, gbc_minth1TF);
		minth1TextField.setColumns(columnOfTextField);
		minth1TextField.setText("10");
		minth1TextField.setEnabled(false);
		AccessControlAlgorith.minthValues.add(minth1TextField);
		
		JLabel maxth2= new JLabel("maxth2:");
		GridBagConstraints gbc_maxth2 = new GridBagConstraints();
		gbc_maxth2.anchor = GridBagConstraints.WEST;
		gbc_maxth2.insets = new Insets(0, 15, 5, 5);
		gbc_maxth2.gridx = 0;
		gbc_maxth2.gridy = 6;
		this.add(maxth2, gbc_maxth2);
		
		maxth2TextField = new JFormattedTextField();
		GridBagConstraints gbc_maxth2TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_maxth2TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxth2TF.gridx = 1;
		gbc_maxth2TF.gridy = 6;
		//gbc_textField.weightx = 2;
		this.add(maxth2TextField, gbc_maxth2TF);
		maxth2TextField.setColumns(columnOfTextField);
		maxth2TextField.setText("70");
		maxth2TextField.setEnabled(false);
		AccessControlAlgorith.maxthValues.add(maxth2TextField);
		
		JLabel minth2= new JLabel("minth2:");
		GridBagConstraints gbc_minth2 = new GridBagConstraints();
		gbc_minth2.anchor = GridBagConstraints.WEST;
		gbc_minth2.insets = new Insets(0, 15, 5, 5);
		gbc_minth2.gridx = 0;
		gbc_minth2.gridy = 7;
		this.add(minth2, gbc_minth2);
		
		minth2TextField = new JFormattedTextField();
		GridBagConstraints gbc_minth2TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_minth2TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minth2TF.gridx = 1;
		gbc_minth2TF.gridy = 7;
		//gbc_textField.weightx = 2;
		this.add(minth2TextField, gbc_minth2TF);
		minth2TextField.setColumns(columnOfTextField);
		minth2TextField.setText("30");
		minth2TextField.setEnabled(false);
		AccessControlAlgorith.minthValues.add(minth2TextField);
		
		JLabel maxth3= new JLabel("maxth3:");
		GridBagConstraints gbc_maxth3 = new GridBagConstraints();
		gbc_maxth3.anchor = GridBagConstraints.WEST;
		gbc_maxth3.insets = new Insets(0, 15, 5, 5);
		gbc_maxth3.gridx = 0;
		gbc_maxth3.gridy = 8;
		this.add(maxth3, gbc_maxth3);
		
		maxth3TextField = new JFormattedTextField();
		GridBagConstraints gbc_maxth3TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_maxth3TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxth3TF.gridx = 1;
		gbc_maxth3TF.gridy = 8;
		//gbc_textField.weightx = 2;
		this.add(maxth3TextField, gbc_maxth3TF);
		maxth3TextField.setColumns(columnOfTextField);
		maxth3TextField.setText("75");
		maxth3TextField.setEnabled(false);
		AccessControlAlgorith.maxthValues.add(maxth3TextField);
		
		JLabel minth3= new JLabel("minth3:");
		GridBagConstraints gbc_minth3 = new GridBagConstraints();
		gbc_minth3.anchor = GridBagConstraints.WEST;
		gbc_minth3.insets = new Insets(0, 15, 5, 5);
		gbc_minth3.gridx = 0;
		gbc_minth3.gridy = 9;
		this.add(minth3, gbc_minth3);
		
		
		minth3TextField = new JFormattedTextField();
		GridBagConstraints gbc_minth3TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_minth3TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minth3TF.gridx = 1;
		gbc_minth3TF.gridy = 9;
		//gbc_textField.weightx = 2;
		this.add(minth3TextField, gbc_minth3TF);
		minth3TextField.setColumns(columnOfTextField);
		minth3TextField.setText("50");
		minth3TextField.setEnabled(false);
		AccessControlAlgorith.minthValues.add(minth3TextField);
		
		JLabel maxth4= new JLabel("maxth4:");
		GridBagConstraints gbc_maxth4 = new GridBagConstraints();
		gbc_maxth4.anchor = GridBagConstraints.WEST;
		gbc_maxth4.insets = new Insets(0, 15, 5, 5);
		gbc_maxth4.gridx = 0;
		gbc_maxth4.gridy = 10;
		this.add(maxth4, gbc_maxth4);
		
		maxth4TextField = new JFormattedTextField();
		GridBagConstraints gbc_maxth4TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_maxth4TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxth4TF.gridx = 1;
		gbc_maxth4TF.gridy = 10;
		//gbc_textField.weightx = 2;
		this.add(maxth4TextField, gbc_maxth4TF);
		maxth4TextField.setColumns(columnOfTextField);
		maxth4TextField.setText("80");
		maxth4TextField.setEnabled(false);
		AccessControlAlgorith.maxthValues.add(maxth4TextField);
		
		JLabel minth4= new JLabel("minth4:");
		GridBagConstraints gbc_minth4 = new GridBagConstraints();
		gbc_minth4.anchor = GridBagConstraints.WEST;
		gbc_minth4.insets = new Insets(0, 15, 5, 5);
		gbc_minth4.gridx = 0;
		gbc_minth4.gridy = 11;
		this.add(minth4, gbc_minth4);
		
		minth4TextField = new JFormattedTextField();
		GridBagConstraints gbc_minth4TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_minth4TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minth4TF.gridx = 1;
		gbc_minth4TF.gridy = 11;
		//gbc_textField.weightx = 2;
		this.add(minth4TextField, gbc_minth4TF);
		minth4TextField.setColumns(columnOfTextField);
		minth4TextField.setText("60");
		minth4TextField.setEnabled(false);
		AccessControlAlgorith.minthValues.add(minth4TextField);
		
		JLabel maxth5= new JLabel("maxth5:");
		GridBagConstraints gbc_maxth5 = new GridBagConstraints();
		gbc_maxth5.anchor = GridBagConstraints.WEST;
		gbc_maxth5.insets = new Insets(0, 15, 5, 5);
		gbc_maxth5.gridx = 0;
		gbc_maxth5.gridy = 12;
		this.add(maxth5, gbc_maxth5);
		
		maxth5TextField = new JFormattedTextField();
		GridBagConstraints gbc_maxth5TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_maxth5TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxth5TF.gridx = 1;
		gbc_maxth5TF.gridy = 12;
		//gbc_textField.weightx = 2;
		this.add(maxth5TextField, gbc_maxth5TF);
		maxth5TextField.setColumns(columnOfTextField);
		maxth5TextField.setText("100");
		maxth5TextField.setEnabled(false);
		AccessControlAlgorith.maxthValues.add(maxth5TextField);
		
		JLabel minth5= new JLabel("minth5:");
		GridBagConstraints gbc_minth5 = new GridBagConstraints();
		gbc_minth5.anchor = GridBagConstraints.WEST;
		gbc_minth5.insets = new Insets(0, 15, 5, 5);
		gbc_minth5.gridx = 0;
		gbc_minth5.gridy = 13;
		this.add(minth5, gbc_minth5);
		
		minth5TextField = new JFormattedTextField();
		GridBagConstraints gbc_minth5TF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_minth5TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_minth5TF.gridx = 1;
		gbc_minth5TF.gridy = 13;
		//gbc_textField.weightx = 2;
		this.add(minth5TextField, gbc_minth5TF);
		minth5TextField.setColumns(columnOfTextField);
		minth5TextField.setText("80");
		minth5TextField.setEnabled(false);
		AccessControlAlgorith.minthValues.add(minth5TextField);
		
		JLabel prawd = new JLabel("prawd. odrzuce�:");
		GridBagConstraints gbc_prawd = new GridBagConstraints();
		gbc_prawd.anchor = GridBagConstraints.WEST;
		gbc_prawd.insets = new Insets(0, 15, 5, 5);
		gbc_prawd.gridx = 0;
		gbc_prawd.gridy = 14;
		this.add(prawd, gbc_prawd);
		
		JTextField prawdTextField = new JFormattedTextField();
		GridBagConstraints gbc_prawdTF = new GridBagConstraints();
		//gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_prawdTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_prawdTF.gridx = 1;
		gbc_prawdTF.gridy = 14;
		//gbc_textField.weightx = 2;
		this.add(prawdTextField, gbc_prawdTF);
		prawdTextField.setColumns(columnOfTextField);
		prawdTextField.setText("0.4");
		AccessControlAlgorith.probValues.add(prawdTextField);
		
		
		InputData id = new InputData(gui);
		id.readData();
		int parametersQuantity = id.distributionParameters.size() / 5;
		for(int i = 1; i<=parametersQuantity; i++){
			chosenLoadParameters.addItem(i);
		}
		
		this.add(chosenLoadParameters, gbc_chosenLoadParameters);
		
		chosenLoadParameters.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				id.importData(chosenLoadParameters.getSelectedItem().toString());
			}
		});
	}
	
	private void setComboboxListener(JComboBox<String> combobox){
		ActionListener  itemListener = new ActionListener () {
	        public void actionPerformed(ActionEvent  itemEvent) {
	    		switch((String)combobox.getSelectedItem()){
					case "RED":	maxth1TextField.setEnabled(true); minth1TextField.setEnabled(true);
						maxth2TextField.setEnabled(false); minth2TextField.setEnabled(false); 
						maxth3TextField.setEnabled(false); minth3TextField.setEnabled(false); 
						maxth4TextField.setEnabled(false); minth4TextField.setEnabled(false);
						maxth5TextField.setEnabled(false); minth5TextField.setEnabled(false); break;
					case "WRED": 	maxth1TextField.setEnabled(true); minth1TextField.setEnabled(true); 
						maxth2TextField.setEnabled(true); minth2TextField.setEnabled(true); 
						maxth3TextField.setEnabled(true); minth3TextField.setEnabled(true); 
						maxth4TextField.setEnabled(true); minth4TextField.setEnabled(true);
						maxth5TextField.setEnabled(true); minth5TextField.setEnabled(true); break;	
					case "RIO":		maxth1TextField.setEnabled(true); minth1TextField.setEnabled(true); 
						maxth2TextField.setEnabled(true); minth2TextField.setEnabled(true); 
						maxth3TextField.setEnabled(false); minth3TextField.setEnabled(false); 
						maxth4TextField.setEnabled(false); minth4TextField.setEnabled(false);
						maxth5TextField.setEnabled(false); minth5TextField.setEnabled(false); break;
					case "Wybierz":		maxth1TextField.setEnabled(false); minth1TextField.setEnabled(false); 
						maxth2TextField.setEnabled(false); minth2TextField.setEnabled(false); 
						maxth3TextField.setEnabled(false); minth3TextField.setEnabled(false); 
						maxth4TextField.setEnabled(false); minth4TextField.setEnabled(false);
						maxth5TextField.setEnabled(false); minth5TextField.setEnabled(false); break;
	    		}
	        	
	        }
	      };
	      combobox.addActionListener(itemListener);
	}
	
	public String getChosenQueuingAlgorithm()
	{
		return (String) chosenQueuingAlgorithm.getSelectedItem();
	}
	
	public String getChosenAccessControlAlgorithm()
	{
		return (String) chosenAccessControlAlgorithm.getSelectedItem();
	}
	
	
}
