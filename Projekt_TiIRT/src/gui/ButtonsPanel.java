package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import core.Main;
import core.Request;
import core.Simulation;
import core.Switch;
import gui.charts.HistogramFrame;
import gui.charts.MultiHistogramFrame;
import gui.charts.WaitingTimeFrame;
import statistics.InputData;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import javax.swing.JLabel;

public class ButtonsPanel extends JPanel
{
	public static JButton start, stop;
	private AlgorithmPanel panel;
	private GUI gui;
	private boolean isStarted = false;
	private JCheckBox isStopAfterTimeCheckBox;
	private JSpinner spinner;
	private Thread thredOfStopActionAfterSpecifiedTime = null;
	private boolean stopByThread = false;
	private JButton histogramBtn;
	private boolean wasStartedOnce = false;
			

	public ButtonsPanel(GUI gui, AlgorithmPanel panel)
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);
		
		this.gui = gui;
		this.panel = panel;
		int marginUpDown = 10;
		
		start = new JButton("START");
		GridBagConstraints gbc_startBtn = new GridBagConstraints();
		gbc_startBtn.anchor = GridBagConstraints.CENTER;
		gbc_startBtn.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_startBtn.gridx = 0;
		gbc_startBtn.gridy = 0;
		start.addActionListener(getStartActionListener());
		//gbc_comboBox.weightx = 3;
		this.add(start, gbc_startBtn);
		
		stop = new JButton("STOP");
		GridBagConstraints gbc_stopBtn = new GridBagConstraints();
		gbc_stopBtn.anchor = GridBagConstraints.CENTER;
		gbc_stopBtn.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_stopBtn.gridx = 1;
		gbc_stopBtn.gridy = 0;
		//gbc_comboBox.weightx = 3;
		stop.addActionListener(getStopActionListener());
		this.add(stop, gbc_stopBtn);
		
		
		isStopAfterTimeCheckBox = new JCheckBox("Czy zatrzymac po:");
		GridBagConstraints gbc_chckbxCzyZatrzymacPo = new GridBagConstraints();
		gbc_chckbxCzyZatrzymacPo.anchor = GridBagConstraints.CENTER;
		gbc_chckbxCzyZatrzymacPo.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_chckbxCzyZatrzymacPo.gridx = 2;
		gbc_chckbxCzyZatrzymacPo.gridy = 0;
		add(isStopAfterTimeCheckBox, gbc_chckbxCzyZatrzymacPo);
		
		SpinnerNumberModel spinnerModel = new SpinnerNumberModel(3, 0, 9999, 1);		
		spinner = new JSpinner(spinnerModel);
		((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setColumns(2);
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.anchor = GridBagConstraints.CENTER;
		gbc_spinner.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_spinner.gridx = 3;
		gbc_spinner.gridy = 0;
		add(spinner, gbc_spinner);
		
		JLabel lblSekundach = new JLabel("Sekundach");
		GridBagConstraints gbc_lblSekundach = new GridBagConstraints();
		gbc_lblSekundach.anchor = GridBagConstraints.CENTER;
		gbc_lblSekundach.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_lblSekundach.gridx = 4;
		gbc_lblSekundach.gridy = 0;
		add(lblSekundach, gbc_lblSekundach);
		
		histogramBtn = new JButton("Get Histogram");
		GridBagConstraints gbc_histBtn = new GridBagConstraints();
		gbc_histBtn.anchor = GridBagConstraints.CENTER;
		gbc_histBtn.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
		gbc_histBtn.gridx = 5;
		gbc_histBtn.gridy = 0;
		//gbc_comboBox.weightx = 3;
		histogramBtn.addActionListener(getHistogramActionActionListener());
		this.add(histogramBtn, gbc_histBtn);
		
	}

	/***
	 * Save all arrival time of all requests to file. This is first step to get histogram of the traffic.
	 * WARNING: Function run in a separate thread - It's prevent from interupated the same thread and prevent from long waiting.
	 * @param requests - all requests arriving to Switch.
	 */
	private void saveHistoryOfCommingAllRequestsToFile(List<Long> requestsTimes){
		new Thread(new Runnable() {		
			@Override
			public void run() {
				if(requestsTimes.isEmpty())
					return;
				
				final long timeOfFirstArrive = requestsTimes.get(0);
				Function<Long, String> getStringOfArrivalTime = (req)-> Long.toString(req -timeOfFirstArrive);
				Stream<String> reqestsTimeStream = requestsTimes.stream().map(getStringOfArrivalTime);
				try {
					Files.write(Paths.get("results\\"+"allRequests.csv"),(Iterable<String>) reqestsTimeStream::iterator);
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
		}).start();
	}
	
	private ActionListener getStartActionListener(){
		return new ActionListener() 
			{				

				@Override
				public void actionPerformed(ActionEvent e) 
				{
					if(isStarted){
						JOptionPane.showMessageDialog(gui.getFrame(),
							    "Symulacja ju� rozpoczeta",
							    "Blad",
							    JOptionPane.ERROR_MESSAGE);
						return;
					}
					wasStartedOnce = true;
					boolean allGeneratorsCorrect = true;
					for(int i=0; i<5; i++){
						int selectedClassCount = gui.getCountOfEachTrafficType().get(i);
						if(selectedClassCount< 0)
							allGeneratorsCorrect= false;
						Simulation.classCount[i] = selectedClassCount;
					}
						
					if(!allGeneratorsCorrect){
						JOptionPane.showMessageDialog(gui.getFrame(),
							    "Za mala ilosc generatorow",
							    "Blad",
							    JOptionPane.ERROR_MESSAGE);
					}
					else{
						isStarted = true;
						InputData id = new InputData(gui);
						id.allData();
						gui.graphPanel.clearDataSet();
						gui.accessLineChartPanel.clearDataSet();
						Main.simulation = new Simulation(gui, panel.getChosenQueuingAlgorithm(), panel.getChosenAccessControlAlgorithm());
						if(isStopAfterTimeCheckBox.isSelected()){
							thredOfStopActionAfterSpecifiedTime = getThredOfStopActionAfterSpecifiedTime();
							thredOfStopActionAfterSpecifiedTime.start();
						}							
					}
				}
			};
	}
	
	private ActionListener getStopActionListener() {
		return new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(!isStarted)
					return;
				
				if(thredOfStopActionAfterSpecifiedTime != null && stopByThread){
					thredOfStopActionAfterSpecifiedTime.interrupt();
					System.out.println("##>" + thredOfStopActionAfterSpecifiedTime.getId());
				}
				
				Main.simulation.interuptAll();
				InputData id = new InputData(gui);
				id.readData();
				InputData.wasParametersLoaded = false;
				//ArrayList<Request> requests =  Main.simulation.getSwitch().getWaitingRequest();
				List<Long> requestsTimes =  Main.simulation.getSwitch().getTimeOfAllArivallPackets();
				saveHistoryOfCommingAllRequestsToFile(requestsTimes);
				isStarted = false;
				thredOfStopActionAfterSpecifiedTime = null;
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						gui.accessBarChartPanel.saveAsImg();
						gui.accessLineChartPanel.saveAsImg();
						gui.accessMultiBarChartPanel.saveAsImg();
						gui.graphPanel.saveAsImg();
					}
				}).start();
			}
		};
	}
	
	private Thread getThredOfStopActionAfterSpecifiedTime(){
		return new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep((int)spinner.getValue()*1000);
					stopByThread = true;
					stop.doClick();
					stopByThread = false;
				} catch (InterruptedException e) {
					System.out.println("Zatrzymnano stopem");
				}			
			}
		});
	}
	
	private ActionListener getHistogramActionActionListener() {
		return new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent e) 
			{	
				if(!wasStartedOnce){
					JOptionPane.showMessageDialog(gui.getFrame(), "Nie wykonano symulacji",
							"Blad", JOptionPane.ERROR_MESSAGE);
					return;
				}
				Switch simulSwitch = Main.simulation.getSwitch();
				List<Long> requestsTimes = simulSwitch.getTimeOfAllArivallPackets();
				List<Long> allowRequestsTimes = simulSwitch.getTimeOfAlowArivallPackets();
				final long timeOfFirstArrive = requestsTimes.get(0);		
				double[] rerestsTimeDouble = prepereRelativeTimeArray(requestsTimes, timeOfFirstArrive);
				double[] allowRerestsTimeDouble = prepereRelativeTimeArray(allowRequestsTimes, timeOfFirstArrive);
				
				List<List<Long>> requestsTimesByPriority = simulSwitch.getTimeOfAllArivallPacketsByPriority();
				List<List<Long>> allowRequestsTimesByPriority = simulSwitch.getTimeOfAllowArivallPacketsByPriority();
				List<double[]> requestsTimesByPriorityDouble = new ArrayList<>();
				List<double[]> allowRequestsTimesByPriorityDouble = new ArrayList<>();
						
				for (int i = 0; i < Switch.PRIORITY_CLASS_COUNT; i++) { //TODO!!
					double[] reqTimeByPriority = prepereRelativeTimeArray(
							requestsTimesByPriority.get(i),		timeOfFirstArrive);
					double[] reqAllowTimeByPriority = prepereRelativeTimeArray(
							allowRequestsTimesByPriority.get(i),timeOfFirstArrive);
					
					requestsTimesByPriorityDouble.add(reqTimeByPriority);
					allowRequestsTimesByPriorityDouble.add(reqAllowTimeByPriority);
				}
			
				
				System.out.println("Wszystie = "+ requestsTimes.size() + ", zaakceptowane"+ allowRequestsTimes.size());
				new HistogramFrame(rerestsTimeDouble,allowRerestsTimeDouble);
				new MultiHistogramFrame(requestsTimesByPriorityDouble, allowRequestsTimesByPriorityDouble);
				new WaitingTimeFrame();
			}
		};
	}
	
	private double[] prepereRelativeTimeArray(List<Long> times, long startTime){
		ToDoubleFunction<Long> changeTimeOfPackagesToSeconds = rawTime -> (rawTime.doubleValue()-startTime)/10e8;
		return times.stream().mapToDouble(changeTimeOfPackagesToSeconds).toArray();	
	}

}
