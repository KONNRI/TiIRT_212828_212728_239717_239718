package gui.charts;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 
import org.jfree.ui.ApplicationFrame; 
import org.jfree.ui.RefineryUtilities; 

public class AccessBarChartPanel extends JPanel{
	private DefaultCategoryDataset dataset;
	private static final String ROW_VALUE = "Wartosc";
	private static final String COLUMN_ACCEPT =  "Przyjete";
	private static final String COLUMN_DENY = "Odrzucone";
	private JFreeChart barChart;
	private String chartTitle;
	
	public AccessBarChartPanel(String chartTitle) {
		this.chartTitle = chartTitle;
		initializeDataSet();

		barChart = ChartFactory.createBarChart(
				chartTitle,
				"Czy odrzucono",
				"Ilo��",
				dataset,
				PlotOrientation.VERTICAL,
				false, true, false);
		//barChart.getXYPlot()
		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
		add(chartPanel);
	}
	
	private void initializeDataSet(){
		this.dataset = new DefaultCategoryDataset();

		dataset.addValue(0L,ROW_VALUE, COLUMN_ACCEPT);
		dataset.addValue(0L,ROW_VALUE, COLUMN_DENY);
	}
	
	public void updateStatisticsOfAccessControll(long accessAllowCout, long accessDenyCout){
		dataset.setValue(accessAllowCout,ROW_VALUE, COLUMN_ACCEPT);
		dataset.setValue(accessDenyCout, ROW_VALUE, COLUMN_DENY);
	}
	
	public void saveAsImg(){
		HistogramSaver.save(barChart, chartTitle+".PNG", 600, 300);
	}
}
