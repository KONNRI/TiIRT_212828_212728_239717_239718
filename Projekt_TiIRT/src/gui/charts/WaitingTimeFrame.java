package gui.charts;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import core.Main;

public class WaitingTimeFrame extends JFrame
{
	private JPanel waitingTimeChartPanel;
	
	public WaitingTimeFrame()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		waitingTimeChartPanel = new WaitingTimeChartPanel("Sredni czas oczekiwania na wykonanie"); 
		waitingTimeChartPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(waitingTimeChartPanel);
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
}
