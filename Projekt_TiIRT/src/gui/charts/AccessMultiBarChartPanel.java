package gui.charts;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


import core.Switch; 

public class AccessMultiBarChartPanel extends JPanel{
	private DefaultCategoryDataset dataset;
	private static final String PRIORITY_GROUP_NANE = "Priorytet ";
	private static final String AC_ACCEPT_NAME =  "Przyjete";
	private static final String AC_DENY_NAME = "Odrzucone";
	private JFreeChart barChart;
	private String chartTitle;
	
	public AccessMultiBarChartPanel(String chartTitle) {
		this.chartTitle = chartTitle;
		initializeDataSet();

		barChart = ChartFactory.createBarChart(
				chartTitle,
				"Czy odrzucono",
				"Procent",
				dataset,
				PlotOrientation.VERTICAL,
				true, true, false);
		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
		add(chartPanel);
	}
	
	private void initializeDataSet(){
		this.dataset = new DefaultCategoryDataset();

		for(int i=0; i< Switch.PRIORITY_CLASS_COUNT; i++){
			dataset.addValue(0L, AC_ACCEPT_NAME, PRIORITY_GROUP_NANE+(i+1));
			dataset.addValue(0L, AC_DENY_NAME,   PRIORITY_GROUP_NANE+(i+1));
		}
	}
	
	public void updateStatisticsOfAccessControll(long[] accessAllowCout, long[] accessDenyCout){
		for(int i=0; i< Switch.PRIORITY_CLASS_COUNT; i++){
			double all = accessAllowCout[i] + accessDenyCout[i];
			double allowPercent = accessAllowCout[i]/all * 100;
			double denyPercent = accessDenyCout[i]/all * 100;
			dataset.setValue(allowPercent, AC_ACCEPT_NAME, PRIORITY_GROUP_NANE+(i+1));
			dataset.setValue(denyPercent,  AC_DENY_NAME,   PRIORITY_GROUP_NANE+(i+1));
		}
	}
	public void saveAsImg(){
		HistogramSaver.save(barChart, chartTitle+".PNG", 600, 300);
	}
}
