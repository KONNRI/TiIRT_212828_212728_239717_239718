package gui.charts;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class HistogramFrame extends JFrame {

	private HistogramChartPanel histogramPanel;

	public HistogramFrame(double[] dataset) {
		histogramPanel = new HistogramChartPanel("Histogram czas�w pojawiania si� pakiet�w", dataset);
		histogramPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(histogramPanel);
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
	
	public HistogramFrame(double[] dataset,double[] datasetAllow) {
		histogramPanel = new HistogramChartPanel("Histogram czas�w pojawiania si� pakiet�w", dataset, datasetAllow);
		histogramPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(histogramPanel);
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
}
