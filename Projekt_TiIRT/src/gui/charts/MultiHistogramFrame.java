package gui.charts;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

public class MultiHistogramFrame extends JFrame {
	private HistogramChartPanel[] histogramPanels;
	private int HEIGHT_OF_ONE_CHART = 250;
	
	public MultiHistogramFrame(List<double[]> listOfListAllRequests, List<double[]> listOfListAllowRequests) {
		super("Histogramy ruchu w zale�nosci od priorytetu");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(800, 800);
		setLocation(150,150);
		JPanel mainPanel = getMainJPanel(listOfListAllRequests, listOfListAllowRequests);
		JScrollPane scrollpane = new JScrollPane(mainPanel);
		getContentPane().add(scrollpane, BorderLayout.CENTER);
		setVisible(true);		
	}

	private JPanel getMainJPanel(List<double[]> listOfListAllRequests, List<double[]> listOfListAllowRequests) {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(5, 1));
		
		histogramPanels = new HistogramChartPanel[5];
		for (int i = 0; i < histogramPanels.length; i++) {
			if(listOfListAllRequests.get(i).length < 1)
				continue;
			HistogramChartPanel actualChartPanel = new HistogramChartPanel(
					"Histogram czas�w pojawiania si� pakiet�w dla priorytetu "+ (i+1), listOfListAllRequests.get(i), listOfListAllowRequests.get(i));		
			actualChartPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			histogramPanels[i] = actualChartPanel;
			actualChartPanel.setSizeOfChartPanel(500, HEIGHT_OF_ONE_CHART);
			actualChartPanel.setDisplayLegend(false);
			mainPanel.add(actualChartPanel);//, gbc_comboBox);
		}
		
		return mainPanel;
	}
	

}
