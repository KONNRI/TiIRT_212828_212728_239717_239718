package gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;

import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


public class AccessLineChartPanel extends JPanel{
	private XYSeriesCollection datasetSerriesAllow;
	private XYSeries datasetAllow;
	private XYSeriesCollection datasetSerriesDeny;
	private XYSeries datasetDeny;
	private boolean isPercent = true;
	private static final String ACCEPT_AXIS_NAME =  "Przyjete";
	private static final String DENY_AXIS_NAME = "Odrzucone";
	private XYPlot xyPlot;
	private Color greenColor = new Color(0, 153, 0);
	private String chartTitle;
	private JFreeChart lineChart;
	
	public AccessLineChartPanel(String chartTitle)
	{
		this.chartTitle = chartTitle;
		initializeDataSet();
		lineChart = ChartFactory.createXYLineChart(
				chartTitle, 
				"Czas",
				(isPercent ? "procent" : "liczba") + " przyjętych",
				datasetSerriesAllow,
				PlotOrientation.VERTICAL,
				true, true, false);
		xyPlot = lineChart.getXYPlot();
		xyPlot.mapDatasetToRangeAxis(1, 1);
		
		final NumberAxis axis2 = new NumberAxis((isPercent ? "procent" : "liczba")+" odrzuconych");
        xyPlot.setRangeAxis(1, axis2);
        xyPlot.setDataset(1, datasetSerriesDeny);
        xyPlot.mapDatasetToRangeAxis(1, 1);
			       
        //final NumberAxis xAxis = (NumberAxis) xyPlot.getDomainAxis();
        //xAxis.setTickUnit(new NumberTickUnit(0.5));        
     
        final StandardXYItemRenderer rendererRed = new StandardXYItemRenderer();
        rendererRed.setSeriesPaint(0, Color.RED);
        final StandardXYItemRenderer rendererGreen = new StandardXYItemRenderer();
        rendererGreen.setSeriesPaint(0, greenColor);
        xyPlot.setRenderer(0, rendererGreen); 
        xyPlot.setRenderer(1, rendererRed);
        
		ChartPanel chart = new ChartPanel(lineChart);
		chart.setPreferredSize(new Dimension(600,300));
		add(chart);
	}
	
	private void initializeDataSet(){	
		datasetSerriesAllow = new XYSeriesCollection();
		datasetSerriesDeny = new XYSeriesCollection();
		
		datasetAllow = new XYSeries(ACCEPT_AXIS_NAME);
		datasetDeny = new XYSeries(DENY_AXIS_NAME);
		
		datasetSerriesDeny.addSeries(datasetDeny);
		datasetSerriesAllow.addSeries(datasetAllow);		
	}
	
	public void addStatisticsOfAccessControll(int accessAllowCout, int accessDenyCout, BigDecimal time){
		double timeD = new Double((time.doubleValue()));
		//System.out.println("DDDDDDDDDDDDD>   "+timeD);
		if(isPercent){
			int allCount = accessAllowCout + accessDenyCout;
			double percentAllow = accessAllowCout/(double)allCount*100;
			datasetAllow.add(timeD,percentAllow);
			datasetDeny.add(timeD,100.0-percentAllow);
		}
		else{
			datasetAllow.add(timeD,accessAllowCout);
			datasetDeny.add(timeD,accessDenyCout);
		}
	}
	public void clearDataSet() {
		datasetAllow.clear();
		datasetDeny.clear();
	}
	
	public void saveAsImg(){
		HistogramSaver.save(lineChart, chartTitle+".PNG", 600, 300);
	}
}
