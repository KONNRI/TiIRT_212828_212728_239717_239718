package gui.charts;

import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

public class HistogramSaver {
	
	public static void save(JFreeChart chart,String name, int width, int height){
		try {
			ChartUtilities.saveChartAsPNG(new File(name), chart, width, height);
		} catch (IOException e) {
			System.err.println("Nie uda�o sie zapisac histogramu do pliku");
		}	
	}
}
