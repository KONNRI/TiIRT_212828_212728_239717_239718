package gui.charts;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import core.Main;
import core.Request;

public class WaitingTimeChartPanel extends JPanel
{
	private String chartTitle;
	private DefaultCategoryDataset dataset;
	private ArrayList<BigDecimal> sumOfWaitingTime;
	private ArrayList<Integer> sumOfRequests;
	private static final String ROW_VALUE = "Wartosc";
	private static final String[] PRIORITY = {"1", "2", "3" , "4", "5"};
	private JFreeChart barChart;
	
	public WaitingTimeChartPanel(String title)
	{
		this.chartTitle = title;
		
		initializeDataset();
		barChart = ChartFactory.createBarChart(
				chartTitle,
				"Priorytety pakiet�w",
				"Czas oczekiwania",
				dataset,
				PlotOrientation.VERTICAL,
				false, true, false);
		
		ChartPanel chartPanel = new ChartPanel(barChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
		add(chartPanel);
		saveChartToFile();
	}
	
	public void initializeDataset()
	{
		this.dataset = new DefaultCategoryDataset();
		
		this.sumOfWaitingTime = new ArrayList<BigDecimal>();
		this.sumOfRequests = new ArrayList<Integer>();
		
		for(String s : PRIORITY)
		{
			dataset.addValue(0L,ROW_VALUE, s);
			sumOfWaitingTime.add(BigDecimal.valueOf(0L));
			sumOfRequests.add(0);
		}
		
		createGraph();
	}
	
	public void createGraph()
	{
		for(Request r : Main.simulation.getSwitch().getExecutedRequest())
		{
			int priorityIndex =  r.getPriority() - 1;
			sumOfWaitingTime.set(priorityIndex, sumOfWaitingTime.get(priorityIndex).add(BigDecimal.valueOf(r.getWaitingTime())));
			sumOfRequests.set(priorityIndex, sumOfRequests.get(priorityIndex) + 1);
		}
		
		ArrayList<BigDecimal> avgWaitingTime = new ArrayList<BigDecimal>();
		
		for(int i = 0; i < PRIORITY.length; i++)
		{
			if(sumOfRequests.get(i) > 0){				
				avgWaitingTime.add(sumOfWaitingTime.get(i).divide(BigDecimal.valueOf(sumOfRequests.get(i)), BigDecimal.ROUND_HALF_UP));		
				dataset.addValue(avgWaitingTime.get(i), ROW_VALUE, PRIORITY[i]);
			}
		}
	}
	private void saveChartToFile() {
		HistogramSaver.save(barChart, chartTitle+".PNG", 600, 300);
	}
	
}
