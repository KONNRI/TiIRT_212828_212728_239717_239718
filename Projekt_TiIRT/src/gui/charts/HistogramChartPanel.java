package gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.io.*;

import javax.swing.JPanel;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.data.statistics.*;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.StandardGradientPaintTransformer;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;

public class HistogramChartPanel extends JPanel{
	private String chartTitle;
	private Color greenColor = new Color(0, 153, 0); 
	private JFreeChart mainChart;
	private int saveImgWidth = 900;
	private int saveImgHeight = 500;
	private HistogramDataset histogramDataset;
	private int numberOfBins;
	private ChartPanel chartPanel;
	
	public HistogramChartPanel(String chartTitle, double[] dataSet) {
		this.chartTitle = chartTitle;
		initializeDataSet(dataSet);
		initializeMainChart();
		changeColorOfBarInChart();	
		saveChartToFile();
		ChartPanel chartPanel = new ChartPanel(mainChart);
		add(chartPanel);
	}
	
	public HistogramChartPanel(String chartTitle, double[] dataSet, double[] allowDataSet) {
		this.chartTitle = chartTitle;
		initializeDataSet(dataSet, allowDataSet);
		initializeMainChart();
		setupChartWith2DataSets();	
		saveChartToFile();
		chartPanel = new ChartPanel(mainChart);
		add(chartPanel);
	}

	public void setSizeOfChartPanel(int width, int height) {
		chartPanel.setSize(width, height);
		chartPanel.setPreferredSize(new Dimension(width, height));
	}
	
	public void setDisplayLegend(boolean flag) {
		mainChart.getLegend().visible = flag;
	}

	private void initializeMainChart() {		
		String plotTitle = this.chartTitle;
		String xaxis = "Czas w sekundach";
		String yaxis = "Ilosc zapytan";
		PlotOrientation orientation = PlotOrientation.VERTICAL;
		boolean show = true;
		boolean toolTips = true;
		boolean urls = false;
		mainChart = ChartFactory.createHistogram(plotTitle, xaxis, yaxis, histogramDataset, orientation, show, toolTips,
				urls);
	}

	private void initializeDataSet(double[] dataSet) {
		numberOfBins = (int)dataSet[dataSet.length-1]*25;
		if(numberOfBins <1)
			numberOfBins=1;
		histogramDataset = new HistogramDataset();
		histogramDataset.setType(HistogramType.FREQUENCY);
		histogramDataset.addSeries("Wszystkie pakiety", dataSet, numberOfBins);
	}
	
	private void initializeDataSet(double[] dataSet, double[] allowDataSet) {
		numberOfBins = (int)dataSet[dataSet.length-1]*25;
		if(numberOfBins <1)
			numberOfBins=1;
		histogramDataset = new HistogramDataset();
		histogramDataset.setType(HistogramType.FREQUENCY);
		if(dataSet != null && dataSet.length > 0)
			histogramDataset.addSeries("Wszystkie pakiety", dataSet, numberOfBins);
		if(allowDataSet != null && allowDataSet.length > 0)
			histogramDataset.addSeries("Zaakceptowane pakiety", allowDataSet, numberOfBins);
	}
	
	private void changeColorOfBarInChart() {
		Plot plot = mainChart.getPlot();
		XYBarRenderer barRenderer = (XYBarRenderer)((XYPlot) plot).getRenderer();
		barRenderer.setSeriesPaint(0, Color.RED);
		barRenderer.setSeriesPaint(1, greenColor);
		barRenderer.setBarPainter(new StandardXYBarPainter());
	}
	
	private void saveChartToFile() {
		HistogramSaver.save(mainChart, chartTitle+".PNG", saveImgWidth, saveImgHeight);
	}
	
	private void setupChartWith2DataSets(){
		 XYPlot plot = (XYPlot) mainChart.getPlot();
		plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
        plot.setDomainPannable(true);
        plot.setRangePannable(true);
        //plot.setForegroundAlpha(0.7f);
        NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
        //yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(1, greenColor);
        renderer.setDrawBarOutline(false);
        renderer.setBarPainter(new StandardXYBarPainter());
        renderer.setShadowVisible(false);
	}
}
