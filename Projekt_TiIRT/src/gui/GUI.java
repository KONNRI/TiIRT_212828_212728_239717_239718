package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import gui.charts.AccessBarChartPanel;
import gui.charts.AccessLineChartPanel;
import gui.charts.AccessMultiBarChartPanel;
import jdistlib.generic.GenericDistribution;
import javax.swing.JTabbedPane;

public class GUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel label;
	private ChooseDistributionsPanel panel;
	public static GraphPanel graphPanel;
	public static AccessBarChartPanel accessBarChartPanel;
	public static AccessMultiBarChartPanel accessMultiBarChartPanel;
	public static AccessLineChartPanel accessLineChartPanel;
	
	private JTabbedPane tabbedPane;

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 920, 600);
		frame.setMinimumSize(new Dimension(800, 600));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		JLabel lab_headingGenerators = new JLabel("Generatory");
		frame.getContentPane().add(lab_headingGenerators, BorderLayout.NORTH);
		
		panel = new ChooseDistributionsPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		AlgorithmPanel panel2 = new AlgorithmPanel(this);
		frame.getContentPane().add(panel2, BorderLayout.WEST);
		
		ButtonsPanel panel3 = new ButtonsPanel(this, panel2);
		//panel3.
		frame.getContentPane().add(panel3, BorderLayout.SOUTH);
		
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			
		graphPanel = new GraphPanel();				
		accessBarChartPanel = new AccessBarChartPanel("Liczba pakietów przyjętych i odrzuconych");
		accessMultiBarChartPanel = new AccessMultiBarChartPanel("Procent pakietów odrzuconych i przyjętych w zależności od priorytetu");
		accessLineChartPanel = new AccessLineChartPanel("Procent pakietów odrzuconych i przyjętych w czasie");
		
		tabbedPane.add("Zapelnienie kolejki", graphPanel);
		tabbedPane.add("Odrzucenia",accessBarChartPanel);
		tabbedPane.add("Odrzucenia by priority",accessMultiBarChartPanel);
		tabbedPane.add("Odrzucenia w czasie",accessLineChartPanel);
		
		
		frame.getContentPane().add(tabbedPane, BorderLayout.EAST);
		//label = new JLabel("0");
		//frame.getContentPane().add(label, BorderLayout.SOUTH);
		
		frame.setVisible(true);
	}
	
	public JLabel getLabel()
	{
		return label;
	}
	
	public ArrayList<GenericDistribution> getChooseDistributionsPanel()
	{
		return panel.getDistributions();
	}
	
	public ArrayList<Integer> getCountOfEachTrafficType()
	{
		ArrayList<Integer> toReturn = new ArrayList<>();
		for(JSpinner s : panel.getCountOfEachTraffic())
			toReturn.add((Integer) s.getValue());
		
		return toReturn;
	}
	
	public JFrame getFrame() {
		return frame;
	}

}
