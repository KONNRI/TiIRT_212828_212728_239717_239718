package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import javafx.scene.control.Spinner;
import jdistlib.generic.GenericDistribution;
import statistics.InputData;


public class ChooseDistributionsPanel extends JPanel {
	private JTextField param1TextField;
	private JTextField param2TextField;
	private ArrayList<DistributionSetterPanel> distributionSetterPanels;
	private ArrayList<JSpinner> countOfEachTrafic;
	
	private static int countOfTraficType = 5;
	private static int numberOfDistributions = 3;
	private String[] namesOfDistributionDestinations= {"Alfa","Beta","Gamma"};
	private String[] namesOfMeaningDestinations= {"Rozmiar Paczki","Odst�py czasu pomiedzy pakietami","Odst�py czasu pomiedzy paczkami"};

	/**
	 * Create the panel.
	 */
	public ChooseDistributionsPanel() {
		
		distributionSetterPanels = new ArrayList<>();
		countOfEachTrafic = new ArrayList<>();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		//gridBagLayout.columnWidths = new int[]{103, 52, 50, 0};
		//gridBagLayout.rowHeights = new int[]{14, 70};
		//gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0};
		//gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		this.setLayout(gridBagLayout);	
		
		JLabel lbl_generatorsCountHeading = new JLabel("Liczba");
		GridBagConstraints gbc_generatorsCountHeading = new GridBagConstraints();
		gbc_generatorsCountHeading.anchor = GridBagConstraints.WEST;
		gbc_generatorsCountHeading.insets = new Insets(0, 0, 5, 5);
		gbc_generatorsCountHeading.gridx = 0;
		gbc_generatorsCountHeading.gridy = 0;			
		this.add(lbl_generatorsCountHeading, gbc_generatorsCountHeading);	
		
		JLabel lbl_generaorsHeading = new JLabel("Generatory");
		GridBagConstraints gbc_generaorsHeading = new GridBagConstraints();
		gbc_generaorsHeading.anchor = GridBagConstraints.CENTER;
		gbc_generaorsHeading.insets = new Insets(0, 0, 5, 5);
		gbc_generaorsHeading.gridx = 1;
		gbc_generaorsHeading.gridy = 0;			
		this.add(lbl_generaorsHeading, gbc_generaorsHeading);	
		
		for(int i=0; i<numberOfDistributions; i++){
			JLabel lblParams = new JLabel(namesOfDistributionDestinations[i]);
			lblParams.setToolTipText(namesOfMeaningDestinations[i]);
			GridBagConstraints gbc_lblKlasaRuchuNr = new GridBagConstraints();
			gbc_lblKlasaRuchuNr.anchor = GridBagConstraints.CENTER;
			gbc_lblKlasaRuchuNr.insets = new Insets(0, 0, 5, 5);
			gbc_lblKlasaRuchuNr.gridx = 2+i;
			gbc_lblKlasaRuchuNr.gridy = 0;
			this.add(lblParams, gbc_lblKlasaRuchuNr);
		}
		
		for(int i=0; i<countOfTraficType; i++){
			SpinnerNumberModel spinnerModel = new SpinnerNumberModel(1, 0, 9999, 1); 
			JSpinner spinner = new JSpinner(spinnerModel);
			((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setColumns(2);			
			GridBagConstraints gbc_spinner = new GridBagConstraints();
			gbc_spinner.gridx = 0;
			gbc_spinner.gridy = i+1;
			add(spinner, gbc_spinner);
			countOfEachTrafic.add(spinner);
			
			JLabel lbl_classOfTraffic = new JLabel(" x Klasa ruchu nr "+(i+1)+":");
			GridBagConstraints gbc_classOfTraffic = new GridBagConstraints();
			gbc_classOfTraffic.anchor = GridBagConstraints.WEST;
			gbc_classOfTraffic.insets = new Insets(0, 0, 5, 5);
			gbc_classOfTraffic.gridx = 1;
			gbc_classOfTraffic.gridy = i+1;			
			this.add(lbl_classOfTraffic, gbc_classOfTraffic);
			
			
			for(int j=0; j<numberOfDistributions; j++){
				DistributionSetterPanel distributionSetterPanel = new DistributionSetterPanel(); // TODO: sparametryzowac
				GridBagConstraints gbc_distributionSetterPanel = new GridBagConstraints();
				gbc_distributionSetterPanel.anchor = GridBagConstraints.CENTER;
				gbc_distributionSetterPanel.insets = new Insets(0, 0, 5, 5);
				gbc_distributionSetterPanel.gridx = 2 + j;
				gbc_distributionSetterPanel.gridy = i+1;
				this.add(distributionSetterPanel, gbc_distributionSetterPanel);
				distributionSetterPanels.add(distributionSetterPanel);
			}
		}
		
		sendSpinners();
	}
	
	private void sendSpinners()
	{
		for(int i = 0; i<countOfEachTrafic.size(); i++){
			InputData.spinnersList.add(countOfEachTrafic.get(i));
		}
	}

	public ArrayList<GenericDistribution> getDistributions(){
		ArrayList<GenericDistribution> distributions = new ArrayList<>();
		for(DistributionSetterPanel panel: distributionSetterPanels){
			distributions.add(panel.getDistributionGenerator());
		}
		return distributions;
	}
	
	public ArrayList<JSpinner> getCountOfEachTraffic()
	{
		return countOfEachTrafic;
	}
}