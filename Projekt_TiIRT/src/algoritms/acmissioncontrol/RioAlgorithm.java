package algoritms.acmissioncontrol;

import java.util.Random;

import core.Generator;
import core.Request;
import core.Simulation;
import core.Switch;

public class RioAlgorithm {
	
	private double avg,m; //sredni rozmiar kolejki, bezczynnosc kolejki / czas transmisji;
	
	//czym wyzszy priorytet tymwazniejszy pakiet
	
	private long maxth1; // maxth dla priorytetu 1
	private long minth1; // minth dla priorytetu 1
	
	private long maxth2; // maxth dla priorytetu 2
	private long minth2; // minth dla priorytetu 2

	private double wq = 0.5; //staly parametr algorytmu RED - na necie jest tlyko ze powinien byc z zakresu od 0 do 1	
	private double r; // losowa z rozkladu normalnego
	
	private double pb,pa; // prawdopodobienstow pb, prawdopodobienstwo pa
	
	private int count = -1; 
	
	public RioAlgorithm(){
		this.avg = 0;
		this.m=0;
		
		this.maxth1 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth1 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth2 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(1).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth2 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(1).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;	
		
	}
	
	private double countAvg(int q){	
		double avgTemp;
		
		if(q!=0){
			avgTemp = (1-wq)*avg + wq*q;
		}else{
			m = (double)(Switch.inActionTime /Generator.workingTime);
			avgTemp = Math.pow(1-wq, m) * avg;
		}		
		
		return avgTemp;
	}
	
	private double countProbability(double maxp, int dsInIp){	
		
		//maxp  jest to prawdopodobienstwo zalezne od wielkosci kolejki
		double pbtemp = maxp * ((avg-(double)returnMinth(dsInIp))/((double)returnMaxth(dsInIp)-(double)returnMinth(dsInIp)));
		return pbtemp;
	}
	
	private double countR(){
		Random random = new Random();
		/*double rand = random.nextGaussian();
		rand = Math.abs(rand);
		if(rand>1){
			rand = 1;
		}
		//double rand = Math.random();
		
		return rand;*/
		double rand = (4 + random.nextGaussian()) / 8;
		if(rand < 0){
			rand = Math.abs(rand);
		}
		
		return rand;
	}
	
	//public boolean whetherReject(int queueSize, int dsInIp){
	public boolean whetherReject(int queueSize, int priority){
		boolean reject;
		int dsInIp = 0;
		
		if(priority == 5 || priority == 4){
			dsInIp = 1; // jezeli priorytet 4 lub 3 to jest wazniejsz, jak nie to do kolejki OUT
		}
		
		avg = countAvg(queueSize);

		if(avg >= returnMinth(dsInIp) && avg < returnMaxth(dsInIp)){
			count++;
			pb = countProbability(Double.parseDouble(AccessControlAlgorith.probValues.get(0).getText()), dsInIp);
			pa=pb/(1- count*pb);
			
			r=countR();
			if(r<pa){
				reject = true;
				count=0;
			}else{
				reject=false;
			}
			
		}else if(avg >= returnMaxth(dsInIp)){
			reject = true; // kazdy pakiet zostaje odrzucony
			count = 0;
		}else{
			reject = false; //przepuszcza dalej pakiet
			count = -1;
		}
		
		return reject;
	}
	
	private long returnMaxth(int dsInIp){
		if(dsInIp == 0){
			return this.maxth1;
		}else{
			return this.maxth2;
		}
	}
	
	private long returnMinth(int dsInIp){
		if(dsInIp == 0){
			return this.minth1;
		}else{
			return this.minth2;
		}
		
	}
}
