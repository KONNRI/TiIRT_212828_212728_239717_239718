package algoritms.acmissioncontrol;

import java.util.ArrayList;

import javax.swing.JTextField;

public class AccessControlAlgorith 
{
	private String algorithm;
	
	private RedAlgorithm red;
	private WRedAlgorithm wred;
	private RioAlgorithm rio;
	
	public static ArrayList<JTextField> maxthValues = new ArrayList<>();
	public static ArrayList<JTextField> minthValues = new ArrayList<>();
	public static ArrayList<JTextField> probValues = new ArrayList<>();
	
	public AccessControlAlgorith(String alg){
		this.algorithm = alg; 
		
		
		this.red = new RedAlgorithm(); 
		this.wred = new WRedAlgorithm();
		this.rio = new RioAlgorithm();
	}
	
	public boolean whetherReject(int queueSize, int priority, int dsInIp){
		boolean bln = true;
		
		if(algorithm == "RED"){
			bln = red.whetherReject(queueSize);
			if(bln){
				System.out.println("ODRZUCA--------------------------------------------------ODRZUCA");
			}else{
				System.out.println("PRZYJMUJE++++++++++++++++++++++++++++++++++++++++++++++++PRZYJMUJE");
			}
		}else if(algorithm == "WRED"){
			bln = wred.whetherReject(queueSize, priority);
			if(bln){
				System.out.println("ODRZUCA--------------------------------------------------ODRZUCA " + priority+ priority+ priority+ priority);
			}else{
				System.out.println("PRZYJMUJE++++++++++++++++++++++++++++++++++++++++++++++++PRZYJMUJE "+ priority+ priority+ priority+ priority);
			}
		}else if(algorithm == "RIO"){
			//bln = rio.whetherReject(queueSize, dsInIp);
			bln = rio.whetherReject(queueSize, priority);
			if(bln){
				System.out.println("ODRZUCA--------------------------------------------------ODRZUCA " + dsInIp+dsInIp+ dsInIp+ dsInIp);
			}else{
				System.out.println("PRZYJMUJE++++++++++++++++++++++++++++++++++++++++++++++++PRZYJMUJE "+ dsInIp+ dsInIp+ dsInIp+ dsInIp);
			}
		}
		
		return bln;
	}
}
