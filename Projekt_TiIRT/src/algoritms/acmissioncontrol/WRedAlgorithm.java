package algoritms.acmissioncontrol;

import java.util.Random;

import core.Generator;
import core.Request;
import core.Simulation;
import core.Switch;

public class WRedAlgorithm {
	
	private double avg,m; //sredni rozmiar kolejki, bezczynnosc kolejki / czas transmisji;
	
	//czym wyzszy priorytet tymwazniejszy pakiet
	
	private long maxth1; // maxth dla priorytetu 1
	private long minth1; // minth dla priorytetu 1
	
	private long maxth2; // maxth dla priorytetu 2
	private long minth2; // minth dla priorytetu 2
	
	private long maxth3; // maxth dla priorytetu 3
	private long minth3; // minth dla priorytetu 3
	
	private long maxth4; // maxth dla priorytetu 4
	private long minth4; // minth dla priorytetu 4
	
	private long maxth5; // maxth dla priorytetu 5
	private long minth5; // minth dla priorytetu 5
	
	private double wq = 0.5; //staly parametr algorytmu RED - na necie jest tlyko ze powinien byc z zakresu od 0 do 1	
	private double r; // losowa z rozkladu normalnego
	
	private double pb,pa; // prawdopodobienstow pb, prawdopodobienstwo pa
	
	private int count = -1; 
	
	public WRedAlgorithm(){
		this.avg = 0;
		this.m=0;
		/*
		this.maxth1 = (50*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth1 = (10*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth2 = (70*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth2 = (30*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth3 = (90*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth3 = (50*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth4 = Simulation.SWITCH_THROUGHPUT/Request.SIZE;
		this.minth4 = (70*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;*/
		
		this.maxth1 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth1 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth2 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(1).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth2 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(1).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth3 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(2).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth3 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(2).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth4 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(3).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth4 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(3).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth5 = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(4).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth5 = (Integer.parseInt(AccessControlAlgorith.minthValues.get(4).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
	}
	
	private double countAvg(int q){	
		double avgTemp;
		
		if(q!=0){
			avgTemp = (1-wq)*avg + wq*q;
		}else{
			m = (double)(Switch.inActionTime /Generator.workingTime);
			avgTemp = Math.pow(1-wq, m) * avg;
		}		
		
		return avgTemp;
	}
	
	private double countProbability(double maxp, int priority){	
		
		//maxp  jest to prawdopodobienstwo zalezne od wielkosci kolejki
		double pbtemp = maxp * ((avg-(double)returnMinth(priority))/((double)returnMaxth(priority)-(double)returnMinth(priority)));
		return pbtemp;
	}
	
	private double countR(){
		Random random = new Random();
		/*double rand = random.nextGaussian();
		rand = Math.abs(rand);
		if(rand>1){
			rand = 1;
		}
		//double rand = Math.random();*/
		
		double rand = (4 + random.nextGaussian()) / 8;
		if(rand < 0){
			rand = Math.abs(rand);
		}
		
		return rand;
	}
	
	public boolean whetherReject(int queueSize, int priority){
		boolean reject;
		
		avg = countAvg(queueSize);

		if(avg >= returnMinth(priority) && avg < returnMaxth(priority)){
			count++;
			pb = countProbability(Double.parseDouble(AccessControlAlgorith.probValues.get(0).getText()), priority);
			pa=pb/(1- count*pb);
			
			r=countR();
			if(r<pa){
				reject = true;
				count=0;
			}else{
				reject=false;
			}
			
		}else if(avg >= returnMaxth(priority)){
			reject = true; // kazdy pakiet zostaje odrzucony
			count = 0;
		}else{
			reject = false; //przepuszcza dalej pakiet
			count = -1;
		}
		
		return reject;
	}
	
	private long returnMaxth(int priority){
		if(priority == 1){
			return this.maxth1;
		}else if( priority == 2){
			return this.maxth2;
		}else if(priority == 3){
			return this.maxth3;
		}else if(priority == 4){
			return this.maxth4;
		}else{
			return this.maxth5;
		}
	}
	
	private long returnMinth(int priority){
		if(priority == 1){
			return this.minth1;
		}else if( priority == 2){
			return this.minth2;
		}else if(priority == 3){
			return this.minth3;
		}else if(priority == 4){
			return this.minth4;
		}else{
			return this.minth5;
		}
	}
}
