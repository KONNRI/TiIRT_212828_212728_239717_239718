package algoritms.acmissioncontrol;

import java.util.Random;

import core.Generator;
import core.Request;
import core.Simulation;
import core.Switch;

public class RedAlgorithm {
	
	private double avg,m; //sredni rozmiar kolejki, bezczynnosc kolejki / czas transmisji;
	
	private long maxth; // aby miec maxymalna dlugosc kolejki trzeba przepustowosc/rozmiar1pakietu
	private long minth; // od 50% zaczynamy odrzucac pakiety z pewnym prawdopodobienstwem
	
	private double wq = 0.5; //staly parametr algorytmu RED - na necie jest tlyko ze powinien byc z zakresu od 0 do 1	
	private double r; // losowa z rozkladu normalnego
	
	private double pb,pa; // prawdopodobienstow pb, prawdopodobienstwo pa
	
	private int count = -1; 
	
	public RedAlgorithm(){
		this.avg = 0;
		this.m=0;
		//this.maxth = Simulation.SWITCH_THROUGHPUT/Request.SIZE;
		//this.minth = (10*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		
		this.maxth = (Integer.parseInt(AccessControlAlgorith.maxthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
		this.minth = (Integer.parseInt(AccessControlAlgorith.minthValues.get(0).getText())*(Simulation.SWITCH_THROUGHPUT/Request.SIZE))/100;
	}
	
	private double countAvg(int q){	
		double avgTemp;
		
		if(q!=0){
			avgTemp = (1-wq)*avg + wq*q;
		}else{
			m = (double)(Switch.inActionTime /Generator.workingTime);
			avgTemp = Math.pow(1-wq, m) * avg;
		}		
		
		return avgTemp;
	}
	
	private double countProbability(double maxp){	
		
		//maxp  jest to prawdopodobienstwo zalezne od wielkosci kolejki
		double pbtemp = maxp * ((avg-(double)minth)/((double)maxth-(double)minth));
		return pbtemp;
	}
	
	private double countR(){
		Random random = new Random();
		/*double rand = random.nextGaussian();
		rand = Math.abs(rand);
		if(rand>1){
			rand = 1;
		}
		//double rand = Math.random();*/
		double rand = (4 + random.nextGaussian()) / 8;
		if(rand < 0){
			rand = Math.abs(rand);
		}
		
		return rand;
	}
	
	public boolean whetherReject(int queueSize){
		boolean reject;
		
		avg = countAvg(queueSize);

		if(avg >= minth && avg < maxth){
			count++;
			pb = countProbability(Double.parseDouble(AccessControlAlgorith.probValues.get(0).getText()));
			pa=pb/(1- count*pb);
			
			r=countR();
			if(r<pa){
				reject = true;
				count=0;
			}else{
				reject=false;
			}
			
		}else if(avg >= maxth){
			reject = true; // kazdy pakiet zostaje odrzucony
			count = 0;
		}else{
			reject = false; //przepuszcza dalej pakiet
			count = -1;
		}
		
		return reject;
	}
	
}
