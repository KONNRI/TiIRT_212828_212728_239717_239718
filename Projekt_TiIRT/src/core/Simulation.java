package core;
import java.util.ArrayList;
import java.util.Iterator;

import gui.GUI;
import jdistlib.generic.GenericDistribution;

public class Simulation 
{
	private GUI gui;
	private Switch sw;
	public ArrayList<ArrayList<Thread>> generators;
	private Thread executeRequesterThread;
	private Thread removeRequesterThread;
	private Thread graphRefresherThread;
	
	private String queuingAlgorithm, accessControlAlgorithm;
	public static int[] classCount = new int[5]; // klasy ruchu - ile w ka�dej wyst�pie�
	public final static long SWITCH_THROUGHPUT = 1000000L; // przepustowo�� switcha 1GB
	public static boolean execute;
	
	public Simulation(GUI gui, String queuingAlgorithm, String accessControlAlgorithm)
	{
		this.gui = gui;
		this.queuingAlgorithm = queuingAlgorithm;
		this.accessControlAlgorithm = accessControlAlgorithm;
		this.generators = new ArrayList<ArrayList<Thread>>();
		execute = true;
		sw = new Switch(SWITCH_THROUGHPUT,queuingAlgorithm, accessControlAlgorithm);
		
		createGenerators();
		Generator.generatorsStartedAd = System.nanoTime();
		startGenerators();
		createQueueManagement();
		//startFirstGenerator();
	}
	// metoda tworz�ca generatory
	public void createGenerators()
	{	
		for(int c = 0; c < 5; c++){
			ArrayList<Thread> generatorThread = new ArrayList<Thread>();
			for(int i = 0; i < classCount[c]; i++) //TODO: class1
			{
				Generator g = new Generator(gui, this, c+1);
				ArrayList<GenericDistribution> distributions = gui.getChooseDistributionsPanel();
				//setupSeed(distributions,c, i); BRAK pozytywnych zmian
				g.setDitributionOfPackSize(distributions.get(c*3+0));
				g.setDitributionOfTimeIntervalBetweenPackages(distributions.get(c*3+1));
				g.setDitributionOfTimeIntervalBetweenPackes(distributions.get(c*3+2));
				Thread t = new Thread(g);
				generatorThread.add(t);
			}
			generators.add(generatorThread);
		}
	}
	
	private void setupSeed(ArrayList<GenericDistribution> distributions, int classType, int numberOfGen){
		for (int i = 0; i < distributions.size(); i++) {
			GenericDistribution dist = distributions.get(i);
			dist.getRandomEngine().setSeed(classType*7171717 + numberOfGen*119+i);
		}
	}
	
	public void createQueueManagement()
	{
		this.executeRequesterThread = new Thread(new ExecuteRequest(gui,this));
		this.executeRequesterThread.start();
		this.removeRequesterThread = new Thread(new RemoveRequest(gui,this));
		this.removeRequesterThread.start();
		
		new Thread(new Counting(sw, 0)).start();
		if(queuingAlgorithm.equals("SJF"))
			new Thread(new Counting(sw, 1)).start();
		this.graphRefresherThread = new Thread(new RefreshGraph(sw));
		this.graphRefresherThread.start();
	}
	
	public void startGenerators(){
		for(ArrayList<Thread> listOfGeneratorsThreads : generators){
			for(Thread generatorThread : listOfGeneratorsThreads){
				generatorThread.start();
			}
		}
	}
	
	public void startFirstGenerator(){
		System.out.println("jestem tutaj");
		generators.get(0).get(0).start();
	}
	
	public Switch getSwitch()
	{
		return sw;
	}
	
	public void interuptAll(){
		for(ArrayList<Thread> listOfGeneratorsThreads : generators){
			for(Thread generatorThread : listOfGeneratorsThreads){
				generatorThread.interrupt();
			}
		}
		this.executeRequesterThread.interrupt();
		this.removeRequesterThread.interrupt();
		this.graphRefresherThread.interrupt();
	}
	
	public String getAccessControlAlgorithm()
	{
		return accessControlAlgorithm;
	}
	
	public String getQueuingAlgorithm()
	{
		return queuingAlgorithm;
	}
	
}
