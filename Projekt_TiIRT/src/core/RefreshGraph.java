package core;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import gui.GUI;

public class RefreshGraph implements Runnable
{
	public static long startTime;
	private Switch sw;
	private ArrayList<Integer> throughput;
	private ArrayList<BigInteger> time;
	private BigDecimal a, b;
	
	public RefreshGraph(Switch sw)
	{
		startTime = System.nanoTime();
		this.sw = sw;
		this.throughput = new ArrayList<>();
		this.time = new ArrayList<>();
	}
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		try
		{
			while(Simulation.execute)
			{
				a = BigDecimal.valueOf(Simulation.SWITCH_THROUGHPUT - sw.getThroughput());
				b = a.divide(BigDecimal.valueOf(Simulation.SWITCH_THROUGHPUT));
				//throughput.add((int) (Simulation.SWITCH_THROUGHPUT - sw.getThroughput())
				System.out.println("Big a: " + a + " b: " + b + " w %: " + b.multiply(BigDecimal.valueOf(100)) + "%");
				Thread.sleep(RemoveRequest.WAITING_TIME);
				long time = System.nanoTime() - startTime;
				a = BigDecimal.valueOf(time);
				//System.out.println("time: " + time + " " + a.divide(BigDecimal.valueOf(1000000000), 3, BigDecimal.ROUND_HALF_UP));
				BigDecimal timeBigDecimal= a.divide(BigDecimal.valueOf(1000000000), 3, BigDecimal.ROUND_HALF_UP);
				//GUI.graphPanel.getDataset().addValue(b.multiply(BigDecimal.valueOf(100)), "w�ze�", timeBigDecimal);
				GUI.graphPanel.updateChart(b.multiply(BigDecimal.valueOf(100)), timeBigDecimal);
				updateAccessBarChart(timeBigDecimal);
			}
		} catch (InterruptedException e) 
		{
            //e.printStackTrace();
			System.out.println("Stop odswierzania grafu");
		}
	}

	private void updateAccessBarChart(BigDecimal time){
		int[] statistics = sw.getAccessControllStatistics();
		List<long[]> statisticsByPriority = sw.getAccessControllStatisticsByPriority();
		GUI.accessBarChartPanel.updateStatisticsOfAccessControll(statistics[0], statistics[1]);
		GUI.accessMultiBarChartPanel.updateStatisticsOfAccessControll(statisticsByPriority.get(0), statisticsByPriority.get(1));
		GUI.accessLineChartPanel.addStatisticsOfAccessControll(statistics[0], statistics[1],time);
	}
}
