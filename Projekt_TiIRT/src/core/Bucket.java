package core;

public class Bucket {
	public static long THROUGHPUT;
	
	public static long REQUEST_SIZE = Request.SIZE;
	
	public static long TOKENS;
	
	public static void calcTokens(){
		TOKENS = THROUGHPUT / REQUEST_SIZE;
	}
}
