package core;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Random;

import gui.GUI;
import jdistlib.generic.GenericDistribution;

public class Generator implements Runnable
{
	private GUI gui;
	private Simulation simulation;
	private long startTime = System.nanoTime(), actualTime, endTime;
	private int trafficType;

	private GenericDistribution ditributionOfTimeIntervalBetweenPackages; //Pakiety
	private GenericDistribution ditributionOfTimeIntervalBetweenPackes;   // Paczki
	private GenericDistribution ditributionOfPackSize;
	public static long generatorsStartedAd, workingTime;

	public double packageTime;
	private int packagesCount;
	private static double onePackegeSize = 32;
	
	
	public Generator(GUI gui, Simulation s, int trafficType)
	{
		this.gui = gui;
		this.simulation = s;
		this.trafficType = trafficType;
	}
	
	@Override
	public void run() 
	{
		try
		{
			actualTime = System.nanoTime();
			
			while(Simulation.execute)
			{
				packagesCount = (int)Math.ceil(ditributionOfPackSize.random() / onePackegeSize);
				System.out.println("Ilosc paczek: "+packagesCount);
				Random r = new Random(); 
				for(int i=0; i< packagesCount; i++){
					long timeIntervalBetweenPackages = (long)(100.0 *ditributionOfTimeIntervalBetweenPackages.random());
					
					System.out.println("Czas odstepu miedzy paczkami" + timeIntervalBetweenPackages);
					Thread.sleep(timeIntervalBetweenPackages);			
					generateRequest(r);
				}
				System.out.println("Pierwsza paczka poszla w czasie "+ (System.nanoTime()-startTime));
				
				long timeIntervalBetweenPackes = (long)(100.0 *ditributionOfTimeIntervalBetweenPackes.random());
				Thread.sleep(timeIntervalBetweenPackes);
				
			}
		}
		catch (InterruptedException e) 
		{
            //e.printStackTrace();
			System.out.println("Zakonczono wykonywanie generatorow");
		}
	}
	

	private void generateRequest(Random r) {
		actualTime = System.nanoTime();
		System.out.println("Jestem w czasie "+ (System.nanoTime()-startTime));
		
		Request request = new Request(System.nanoTime(), trafficType, trafficType); // priorytet tak jak klasa ruchu
		simulation.getSwitch().addGeneratedRequest(request);
		simulation.getSwitch().addToQueue(request);
	}
	
	public void setDitributionOfTimeIntervalBetweenPackages(GenericDistribution ditributionOfTimeIntervalBetweenPackages) {
		this.ditributionOfTimeIntervalBetweenPackages = ditributionOfTimeIntervalBetweenPackages;
	}
	public void setDitributionOfTimeIntervalBetweenPackes(GenericDistribution ditributionOfTimeIntervalBetweenPackes) {
		this.ditributionOfTimeIntervalBetweenPackes = ditributionOfTimeIntervalBetweenPackes;
	}
	public void setDitributionOfPackSize(GenericDistribution ditributionOfPackSize) {
		this.ditributionOfPackSize = ditributionOfPackSize;
	}
	
	
}
