package core;

public class Request 
{
	public static final long EXECUTION_TIME = 100; // czas wykonania sta�y 0.1s
	private long arrivalTime, timeToEnd, endTime, waitingTime;
	private int priority, trafficType, dsInIp;
	public static final long SIZE = 5000;
	private Boolean isAccepted = null;
	
	public Request(long arrivalTime, int priority, int trafficType)
	{
		this.arrivalTime = arrivalTime;
		this.timeToEnd = EXECUTION_TIME;
		this.priority = priority;
		this.trafficType = trafficType;
		
		this.dsInIp = (Math.random()<0.5)?0:1;
	}
	
	public Request() 
	{
		this.arrivalTime = 0;
		this.timeToEnd = 0;
		this.priority = 0;
		
		this.dsInIp = (Math.random()<0.5)?0:1;
	}
	
	public int getDsInIp(){
		return dsInIp;
	}

	public long getArrivalTime() 
	{
		return arrivalTime;
	}

	public long getTimeToEnd() 
	{
		return timeToEnd;
	}

	public int getPriority() 
	{
		return priority;
	}
	
	public int getTrafficType()
	{
		return trafficType;
	}
	
	public void setTimeToEnd(long time)
	{
		this.timeToEnd = time;
	}
	
	public void setWaitingTime(long endTime)
	{
		this.endTime = endTime;
		waitingTime = endTime - arrivalTime;
	}
	
	public long getWaitingTime()
	{
		return waitingTime;
	}
	
	public Boolean getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
}
