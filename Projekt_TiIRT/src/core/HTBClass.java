package core;
import java.util.ArrayList;

public class HTBClass {

	private long r;
	private long ar;
	private long cr;
	private int p;
	
	public ArrayList<Long> borrowed;
	public ArrayList<Integer> borrowedFrom;
	public long shared;
	
	public String mode;
	
	public HTBClass(){}
	
	public HTBClass(int priority,long r, long ar, long cr){
		this.p = priority;
		this.ar = ar;
		this.cr = cr;
		this.r = r;
		
		this.borrowedFrom = new ArrayList<Integer>();
		this.borrowed = new ArrayList<Long>();
		this.shared = 0;
	}
	
	public String checkMode(){
		if(r > cr){
			mode = "R"; //nie moze wysylac
		}else if( r <= cr && r >= ar){//this.getArWithVariables() > ar){
			mode = "Y"; // klasa moze pozyczac
		}else{
			mode = "G"; // moze wysylac
		}
		
		return mode;
	}
	
	public void currentQueueSize(){}
	
	public long shareTroughput(long howMuch){
		if(borrowed.size() > 0){
			shared = 0;
			return shared;
		}else{
			shared = ar - r;
			if(shared < 0){
				shared = 0;
			}
			
			if(shared > cr - ar){
				shared = cr - ar;
			}
			
			if(shared > howMuch){
				shared = howMuch;
			}
			
			return shared;
		}
		
	}
	
	public void resetShares(){
		borrowed = new ArrayList<Long>();
		borrowedFrom = new ArrayList<Integer>();
		shared = 0;
	}
	
	public void borrowThroughput(int id, long howMuch){
		borrowed.add(howMuch);
		borrowedFrom.add(id);
	}
	
	public long backBorrowed(int id){
		long howMuch = 0;
		for(int i = 0; i< borrowedFrom.size(); i++){
			if(borrowedFrom.get(i) == id){
				howMuch = borrowed.get(i);
				borrowed.remove(i);
			}
		}
		
		return howMuch;
	}
	
	public long getAr(){
		return ar;
	}
	
	public long getArWithVariables(){
		long borrow = 0;
		for(int i = 0; i<borrowed.size(); i++){
			borrow += borrowed.get(i);
		}
		
		return ar+borrow-shared;
	}
	
	public void setR(long r){
		this.r = r;
	}
	
	public long getR(){
		return this.r;
	}
	
	public long getCr(){
		return this.cr;
	}
}
