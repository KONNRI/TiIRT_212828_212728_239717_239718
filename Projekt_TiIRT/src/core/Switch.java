package core;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import algoritms.acmissioncontrol.AccessControlAlgorith;
import algoritms.acmissioncontrol.RedAlgorithm;

public class Switch 
{
	private long throughput; // przepustowo�� (ile mo�e jeszcze wykorzysta�)
	
	private ArrayList<ArrayList<Request>> queues; 	// kolejki u�ywane w algorytmie WFQ, SJF, HTB
	private ArrayList<Request> waitingRequests;		// kolejka u�ywana w algorytmach FIFO, PQ
	private ArrayList<Request> executingRequests;	
	private ArrayList<Request> executedRequests; // ��dania w kolejce i wykynywane
	private ArrayList<Request> accessDeniedRequests;
	private ArrayList<Request> accessAcceptedRequests;

	
	private String queuing; // kolejkowanie
	private String accessControllAlgorithm; // algorytm kontroli dost�pu
	private double[] usedQ;
	private double[] maxQ;
	public static long inActionTime = 0, lastEmpty = 0;
	private ArrayList<Integer> numberOfGeneratedPackets, lastNumberOfGeneratedPackets; // ilo�� pakiet�w generowanych przez klasy ruchu
	private boolean canCompare; // sprawdzenie czy mo�na por�na� ze sob� ilo�� generowanych pakiet�w z r�nych charakterystyk ruchu
	private int accessedRequests;
	private AccessControlAlgorith alg;
	private ArrayList<HTBClass> htbs;
	private long[] countOfAllowRequestsByPriority;
	private long[] countOfDenyRequestsByPriority;
	
	public static int PRIORITY_CLASS_COUNT = 5;	
	
	
	public Switch(long throughput, String queuingAlgorithm, String accessControlAlgorithm)
	{
		this.throughput = throughput;
		this.executingRequests = new ArrayList<Request>();
		this.waitingRequests = new ArrayList<Request>();
		this.executedRequests = new ArrayList<Request>();
		this.accessDeniedRequests = new ArrayList<Request>();
		this.accessAcceptedRequests = new ArrayList<>();
		this.queues = new ArrayList<ArrayList<Request>>();
		
		setNumberOfGeneratedPackets();
		
		for(int i = 0; i < 5; i++)
		{
			queues.add(new ArrayList<Request>());
		}
		
		this.queuing = queuingAlgorithm;
		this.accessControllAlgorithm = accessControlAlgorithm;
		this.alg = new AccessControlAlgorith(this.accessControllAlgorithm);
		setWeightsWFQ();
		
		this.accessedRequests = 0;// liczba zaakceptowanych pakietow
		
		this.htbs = new ArrayList<HTBClass>();
		Bucket.THROUGHPUT = throughput;
		Bucket.calcTokens();
		
		for(int i = 1; i <= 5; i++)
		{
			this.htbs.add(new HTBClass(i, 0, (15*Bucket.TOKENS)/100,(15*Bucket.TOKENS)/100 + (20*Bucket.TOKENS)/100));
		}
		countOfAllowRequestsByPriority = new long[PRIORITY_CLASS_COUNT];
		countOfDenyRequestsByPriority = new long[PRIORITY_CLASS_COUNT];
	}
	
	public Switch() {};

	public void setNumberOfGeneratedPackets()
	{
		numberOfGeneratedPackets = new ArrayList<Integer>();
		lastNumberOfGeneratedPackets = new ArrayList<Integer>();
		canCompare = false;
		
		for(int i = 0; i < 5; i++)
		{
			numberOfGeneratedPackets.add(0);
			lastNumberOfGeneratedPackets.add(0);
		}
	}
	
	public void setCanCompare(boolean canCompare)
	{
		this.canCompare = canCompare;
	}
	
	public long getThroughput() 
	{
		return throughput;
	}

	public ArrayList<Request> getWaitingRequest() 
	{
		return waitingRequests;
	}

	public ArrayList<Request> getExecutingRequest() 
	{
		return executingRequests;
	}
	
	public ArrayList<Request> getExecutedRequest()
	{
		return executedRequests;
	}
	public ArrayList<ArrayList<Request>> getQueues()
	{
		return queues;
	}
	
	public void setThroughput(long value)
	{
		this.throughput = value;
	}
	
	public ArrayList<Integer> getNumberOfGeneratedPackets()
	{
		return numberOfGeneratedPackets;
	}
	
	public ArrayList<Integer> getLastNumberOfGeneratedPackets()
	{
		return lastNumberOfGeneratedPackets;
	}
	
	public void setWeightsWFQ()
	{
		usedQ = new double[5];
		maxQ = new double[5];
		
		double a;
		double b = 15;
		double c;
		
		for (int i = 0; i < usedQ.length; i++) {
			usedQ[i]=0;
			
			a = i+1;
			c=a/b;
			maxQ[i] = c * throughput;
		}
	}
	
	private boolean controllAccess(Request request){
		
		boolean isRejected = false;
		
		if(queuing.equals("FIFO") || queuing.equals("PQ")
				|| queuing.equals("SJF") || queuing.equals("WFQ") || queuing.equals("HTB")){

			isRejected = alg.whetherReject(executingRequests.size(),request.getPriority(), request.getDsInIp());
			if(isRejected){
				accessDeniedRequests.add(request);
				System.out.println("Liczba odrzuconych pakiet�w " + accessDeniedRequests.size());
				System.out.println("Liczba zaakceptowanych pakiet�w " + accessedRequests);
				countOfDenyRequestsByPriority[request.getPriority()-1]++;
			}else{
				accessAcceptedRequests.add(request);
				accessedRequests++;
				countOfAllowRequestsByPriority[request.getPriority()-1]++;
			}
		}
		request.setIsAccepted(!isRejected);
		return isRejected;
	}
	
	// ustawianie ile w ostatnim czasie przysz�o pakiet�w z danej klasy ruchu
	synchronized public void addGeneratedRequest(Request request)
	{
		if(request == null)
		{
			for(int i = 0; i < numberOfGeneratedPackets.size(); i++)
				numberOfGeneratedPackets.set(i, 0);
		}
		else
			numberOfGeneratedPackets.set(request.getTrafficType() - 1, numberOfGeneratedPackets.get(request.getTrafficType() - 1) + 1);
	}
	
	synchronized public void addToQueue(Request request)
	{
		if(request == null)
			throw new IllegalArgumentException("Error! Null requests are not allowed.");
		
		// sprawdzamy czy przepuscimy pakiet
		if(!controllAccess(request)){
			if(queuing.equals("FIFO"))
			{ 			
				waitingRequests.add(request);			
			}
			else if(queuing.equals("SJF"))
			{
				int trafficType = request.getTrafficType();
				queues.get(trafficType-1).add(request);		
			}
			else if(queuing.equals("PQ"))
			{
				int position = -1;
				for(int i = 0; i < waitingRequests.size(); i++)
				{
					if(request.getPriority() > waitingRequests.get(i).getPriority())
					{
						position = i;
						break;
					} else
					{
						// je�eli r�wne priorytety, to pierwszy wykona si� ten, co ma kr�tszy czas wykonania
						if(request.getPriority() == waitingRequests.get(i).getPriority())
						{
							if(request.getArrivalTime() < waitingRequests.get(i).getArrivalTime())
							{
								position = i;
								break;
							}
						}
					}
				}
				
				if(position != -1)
					waitingRequests.add(position, request);
				else
					waitingRequests.add(request);
			} 
			else if(queuing.equals("WFQ"))
			{	
				// dodanie ��da� do odpowiednich kolejek ze wzgl�du na priorytety, potem b�dzie pobieranie z tych kolejek i wrzucanie do wykonywania
				
				//int trafficType = request.getTrafficType();
				queues.get(request.getPriority()-1).add(request);
			}
			else if(queuing.equals("HTB"))
			{
				//int trafficType = request.getTrafficType();
				queues.get(request.getPriority()-1).add(request);									
			}
		}// controllAccess() - koniec ifa
	}
	
	synchronized public void executeRequestFromQueue()
	{
		// bierzemy element z kolejki oczekuj�cych i dodajemy do wykonywanych
		if((queuing.equals("FIFO") || queuing.equals("PQ")))
		{
			while(getWaitingRequest().size() > 0 && getThroughput() - Request.SIZE >= 0)
			{
				// przerzucamy pierwszy element z kolejki oczekuj�cych pakiet�w do wykonywanych
				getExecutingRequest().add(getWaitingRequest().get(0));
				getWaitingRequest().get(0).setWaitingTime(System.nanoTime());
				getWaitingRequest().remove(0);
				setThroughput(getThroughput() - Request.SIZE); 
			}
		}
		else if(queuing.equals("SJF"))
		{
			int count = 0;
			while(getThroughput() - Request.SIZE >= 0 && (getQueues().get(0).size() > 0 || getQueues().get(1).size() > 0 
					|| getQueues().get(2).size() > 0 || getQueues().get(3).size() > 0 || getQueues().get(4).size() > 0))
			{
				// wybieramy t� klase ruchu, kt�ra przez ostatnie 2s generuje najmniejszy ruch
				if(canCompare)
				{
					int minTraffic = Integer.MAX_VALUE, indication = -1;
					for(int i = 0; i < 5; i++)
					{
						if(lastNumberOfGeneratedPackets.get(i) < minTraffic && getQueues().get(i).size() > 0)
							indication = i;
					}
					if(indication != -1)
					{
						getExecutingRequest().add(getQueues().get(indication).get(0));
						getQueues().get(indication).get(0).setWaitingTime(System.nanoTime());
						getQueues().get(indication).remove(0);
						setThroughput(getThroughput() - Request.SIZE); 
					}
				} else
				{
					if(getQueues().get(count).size() > 0)
					{
						getExecutingRequest().add(getQueues().get(count).get(0));
						getQueues().get(count).get(0).setWaitingTime(System.nanoTime());
						getQueues().get(count).remove(0);
						setThroughput(getThroughput() - Request.SIZE);
					}
					count++;
					if(count == getQueues().size())
						count = 0;
				}
			}
		} 
		else if(queuing.equals("WFQ"))
		{
			while(throughput - Request.SIZE >= 0 && 
					  ((getQueues().get(0).size() > 0 && usedQ[0] + Request.SIZE <= maxQ[0])
					|| (getQueues().get(1).size() > 0 && usedQ[1] + Request.SIZE <= maxQ[1])
					|| (getQueues().get(2).size() > 0 && usedQ[2] + Request.SIZE <= maxQ[2])
					|| (getQueues().get(3).size() > 0 && usedQ[3] + Request.SIZE <= maxQ[3])
					|| (getQueues().get(4).size() > 0 && usedQ[4] + Request.SIZE <= maxQ[4])))
			{
				for(int i = 0; i < 5; i++)
				{
					if(getQueues().get(i).size() > 0)
					{
						if(usedQ[i] + Request.SIZE <= maxQ[i])
						{
							usedQ[i] += Request.SIZE;
							executingRequests.add(getQueues().get(i).get(0));
							getQueues().get(i).get(0).setWaitingTime(System.nanoTime());
							getQueues().get(i).remove(0);
							setThroughput(throughput - Request.SIZE);
						}
					}
				}
			}
		}
		else //HTB
		{ 		
			borrowTokens();
			while(throughput - Request.SIZE >= 0 && 
					  ((getQueues().get(0).size() > 0 && usedQ[0] + Request.SIZE <= htbs.get(0).getArWithVariables() * Request.SIZE)
					|| (getQueues().get(1).size() > 0 && usedQ[1] + Request.SIZE <= htbs.get(1).getArWithVariables() * Request.SIZE)
					|| (getQueues().get(2).size() > 0 && usedQ[2] + Request.SIZE <= htbs.get(2).getArWithVariables() * Request.SIZE)
					|| (getQueues().get(3).size() > 0 && usedQ[3] + Request.SIZE <= htbs.get(3).getArWithVariables() * Request.SIZE)
					|| (getQueues().get(4).size() > 0 && usedQ[4] + Request.SIZE <= htbs.get(4).getArWithVariables() * Request.SIZE)))
			{
				
				for(int i= 0; i<5; i++)
				{
					
					if(getQueues().get(i).size() > 0)
					{
						if(usedQ[i] + Request.SIZE <= htbs.get(i).getArWithVariables() * Request.SIZE)
						{
							usedQ[i] += Request.SIZE;
							executingRequests.add(getQueues().get(i).get(0));
							getQueues().get(i).get(0).setWaitingTime(System.nanoTime());
							getQueues().get(i).remove(0);
							setThroughput(throughput - Request.SIZE);
							htbs.get(i).setR((long)usedQ[i]);
						}
					}
					
				}
				
			}	
			deleteShares();	
		}
	}
		
	synchronized public void removeRequestFromQueue()
	{
		Iterator<Request> it = getExecutingRequest().iterator();
		while(it.hasNext())
		{
			Request r = (Request) it.next();
			if(r.getTimeToEnd() <= 0)
			{
				it.remove();
				executedRequests.add(r);
				if(queuing.equals("WFQ"))
				{
					int priority = r.getPriority();
					usedQ[priority-1] -= Request.SIZE;
				}
				
				if(queuing.equals("HTB"))
				{
					int priority = r.getPriority();
					int priorityIndex = priority-1;
					usedQ[priorityIndex] -= Request.SIZE;
					htbs.get(priorityIndex).setR((long)usedQ[priorityIndex]);
				}
				setThroughput(getThroughput() + Request.SIZE);
			} else
			{
				r.setTimeToEnd(r.getTimeToEnd() - RemoveRequest.WAITING_TIME);
			}
		}
	}
	
	public void deleteShares(){
		for(int i=0; i<htbs.size(); i++){
			htbs.get(i).resetShares();
		}
	}
	
	public void borrowTokens(){
		for(int i = 0; i<htbs.size(); i++){
			for(int j = 0; j<htbs.size(); j++){
				if(i != j){
					if(htbs.get(i).getArWithVariables() < htbs.get(i).getCr() && checkHtbsMode(j) == "G" &&  checkHtbsMode(i) == "Y"){
						htbs.get(i).borrowThroughput(j, htbs.get(j).shareTroughput(htbs.get(i).getCr() - htbs.get(i).getArWithVariables()));
					}
				}
			}
		}
	}
	
	public String checkHtbsMode(int i){
		return htbs.get(i).checkMode();
	}

	
	public double getUsedQ1()
	{
		return usedQ[0];
	}
	
	public double getUsedQ2()
	{
		return usedQ[1];
	}
	
	public double getUsedQ3()
	{
		return usedQ[2];
	}
	
	public double getUsedQ4()
	{
		return usedQ[3];
	}
	
	public double getUsedQ5()
	{
		return usedQ[4];
	}
	
	public int[] getAccessControllStatistics(){
		return new int[]{accessAcceptedRequests.size(), accessDeniedRequests.size()};
	}
	
	public synchronized List<Long> getTimeOfAllArivallPackets(){
		Stream<Request> allReqestStream = Stream.concat(this.accessAcceptedRequests.stream(), this.accessDeniedRequests.stream());
		Stream<Long> arrivalTimesLongStream = allReqestStream.map(r -> new Long(r.getArrivalTime()));
		return arrivalTimesLongStream.collect(Collectors.toCollection(ArrayList::new));
	}
	
	public synchronized List<Long> getTimeOfAlowArivallPackets(){
		Stream<Long> arrivalTimesLongStream = this.accessAcceptedRequests.stream().map(r -> new Long(r.getArrivalTime()));
		return arrivalTimesLongStream.collect(Collectors.toCollection(ArrayList::new));
	}
	
	public List<List<Long>> getTimeOfAllArivallPacketsByPriority(){
		Stream<Request> allReqestStream = Stream.concat(this.accessAcceptedRequests.stream(), this.accessDeniedRequests.stream());
		ArrayList<List<Long>> listOfListTimes =  new ArrayList<>();
		for(int i=0; i<5; i++){
			listOfListTimes.add(new ArrayList<>());
		}
		allReqestStream.forEach(r-> listOfListTimes.get(r.getPriority()-1).add(r.getArrivalTime()));
		return listOfListTimes;
	}
	
	public List<List<Long>> getTimeOfAllowArivallPacketsByPriority(){
		Stream<Request> reqestStream = accessAcceptedRequests.stream();
		ArrayList<List<Long>> listOfListTimes =  new ArrayList<>();
		for(int i=0; i<5; i++){
			listOfListTimes.add(new ArrayList<>());
		}
		reqestStream.forEach(r-> listOfListTimes.get(r.getPriority()-1).add(r.getArrivalTime()));
		return listOfListTimes;
	}
	
	public List<long[]> getAccessControllStatisticsByPriority(){
		ArrayList<long[]> list = new ArrayList<>();
		list.add(countOfAllowRequestsByPriority);
		list.add(countOfDenyRequestsByPriority);
		return list;
	}
	
}
