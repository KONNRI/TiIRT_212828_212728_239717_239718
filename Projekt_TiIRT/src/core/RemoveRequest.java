package core;

import java.util.Iterator;

import gui.GUI;

public class RemoveRequest implements Runnable
{
	private GUI gui;
	private Simulation s;
	public static int WAITING_TIME = 25;
	
	public RemoveRequest(GUI gui, Simulation s)
	{
		this.gui = gui;
		this.s = s;
	}
	
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		try 
		{
			while(Simulation.execute)
			{	// je�eli FIFO lub PQ to pobierz z og�lnej kolejki, je�eli inaczej to z kolejek wzgl�dem klas ruchu
				// b�dz priorytetu
				s.getSwitch().removeRequestFromQueue();
				/*
				Iterator<Request> it = s.getSwitch().getExecutingRequest().iterator();
				while(it.hasNext())
				{
					Request r = (Request) it.next();
					if(r.getTimeToEnd() <= 0)
					{
						it.remove();
						s.getSwitch().setThroughput(s.getSwitch().getThroughput() + Request.SIZE);
					} else
					{
						r.setTimeToEnd(r.getTimeToEnd() - 100);
					}
				}*/
				// co 100 milisekund ma sprawdza� kolejk�
				Thread.sleep(WAITING_TIME);
			}
		} catch (InterruptedException e) 
		{
            //e.printStackTrace();
		}
	}

}
