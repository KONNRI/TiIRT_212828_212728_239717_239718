package core;
import java.awt.EventQueue;

import javax.swing.JLabel;

public class Counting implements Runnable
{
	private Switch sw;
	private int kind;
	
	public Counting(Switch sw, int kind)
	{
		this.sw = sw;
		this.kind = kind;
	}
	@Override
	public void run() 
	{
		try
		{
			if(kind == 0)
			{
				Generator.workingTime = System.nanoTime() - Generator.generatorsStartedAd;
			
				if(sw.getExecutingRequest().size() == 0)
					Switch.lastEmpty = System.nanoTime();
			
				Switch.inActionTime = System.nanoTime() - Switch.lastEmpty;
				
				Thread.sleep(50);
			} else
			{
				Thread.sleep(2000);
				
				for(int i = 0; i < sw.getLastNumberOfGeneratedPackets().size(); i++)
				{
					sw.getLastNumberOfGeneratedPackets().set(i, sw.getNumberOfGeneratedPackets().get(i));
				}
				
				sw.setCanCompare(true);
				sw.addGeneratedRequest(null);
			}
		} catch (InterruptedException e) 
		{
            e.printStackTrace();
        }
		
	}

}
