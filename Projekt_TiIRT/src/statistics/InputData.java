package statistics;

import gui.GUI;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import org.json.simple.JSONObject;
import org.json.simple.parser.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Paths;

public class InputData
{
  public static ArrayList<JComboBox<String>> comboBoxDistributionValues = new ArrayList<>();
  public static ArrayList<JTextField> textFieldsDistributionValues = new ArrayList<>();
  public static ArrayList<JSpinner> spinnersList = new ArrayList<>();
  
  public static boolean wasParametersLoaded = false;
  
  private GUI gui;
  private int[] classes = {0, 0, 0, 0, 0};
  
  private ArrayList<Coefficients> coefficients = new ArrayList<>();
  
  public ArrayList<String> distributionParameters = new ArrayList<>();
  
  private String path = Paths.get("results\\inputData.txt").toString();
  class Coefficients{
	  public String valueCB;
	  
	  public String value1;
	  public String value2;
	  public String value3;
	  
	  public Coefficients(String valCB, String val1, String val2, String val3){
		  this.valueCB = valCB;
		  this.value1 = val1;
		  this.value2 = val2;
		  this.value3 = val3;
	  }
  }  
  
  public InputData(GUI gui) {
	  this.gui = gui;	  
  }
  
  public void allData()
  {
	  if(!wasParametersLoaded){
		  classes[0] = gui.getCountOfEachTrafficType().get(0);
		  classes[1] = gui.getCountOfEachTrafficType().get(1);
		  classes[2] = gui.getCountOfEachTrafficType().get(2);
		  classes[3] = gui.getCountOfEachTrafficType().get(3);
		  classes[4] = gui.getCountOfEachTrafficType().get(4);
		  
		  int j = 0;
		  for(int i = 0; i<15; i++){
			  String val1 = comboBoxDistributionValues.get(i).getSelectedItem().toString();
			  
			  String val2 = textFieldsDistributionValues.get(j).getText();
			  j++;
			  String val3 = textFieldsDistributionValues.get(j).getText();
			  j++;
			  String val4 = textFieldsDistributionValues.get(j).getText();
			  j++;
			  
			  coefficients.add(new Coefficients(val1, val2, val3, val4));
		  }
		  
		  writeAllData();
	  }
  }
  
  public void writeAllData()
  {
	  ArrayList<JSONObject> listObjects = new ArrayList<>();
	  
	  int j = 0;
	  for(int i = 0; i<classes.length; i++){
		  JSONObject obj = new JSONObject();
		  
		  obj.put("class-id", i+1);
		  obj.put("quantity", classes[i]);
		  
		  obj.put("alfa-cb", coefficients.get(j).valueCB.toString());
		  obj.put("alfa-v1", coefficients.get(j).value1.toString());
		  obj.put("alfa-v2", coefficients.get(j).value2.toString());
		  obj.put("alfa-v3", coefficients.get(j).value3.toString());
		  j++;
		  
		  obj.put("beta-cb", coefficients.get(j).valueCB.toString());
		  obj.put("beta-v1", coefficients.get(j).value1.toString());
		  obj.put("beta-v2", coefficients.get(j).value2.toString());
		  obj.put("beta-v3", coefficients.get(j).value3.toString());
		  j++;
		  
		  obj.put("gamma-cb", coefficients.get(j).valueCB.toString());
		  obj.put("gamma-v1", coefficients.get(j).value1.toString());
		  obj.put("gamma-v2", coefficients.get(j).value2.toString());
		  obj.put("gamma-v3", coefficients.get(j).value3.toString());
		  j++;
		  
		  listObjects.add(obj);
	  }
	  
	  
	  try(FileWriter fw = new FileWriter(path, true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
			{
		  
		  		for(int i=0; i<listObjects.size(); i++){
		  			StringWriter out2 = new StringWriter();
		  			listObjects.get(i).writeJSONString(out2);
		  	      
		  			String jsonText = out2.toString();
		  			out.println(jsonText);
		  		}
			    
		  		//fw.flush();
				//fw.close();
			} catch (IOException e) {
			    e.printStackTrace();
			}
  }
  
  public void readData(){
	 
	  
	  distributionParameters.clear();
	  
	  try{		  
		  FileReader fr = new FileReader(path);
		  BufferedReader br = new BufferedReader(new FileReader(path));
		  
		  String getLine = br.readLine();
		  
		  while(getLine != null)
		  {
			  distributionParameters.add(getLine);
			  getLine = br.readLine();			  
		  }
		  
		  
	  }catch (FileNotFoundException e){
		  e.printStackTrace();
	  }catch (IOException e){
		    System.out.println(e);
	  }
  }
  
  public void importData(String id){
	  if(id != "Wybierz"){
		  int params = Integer.parseInt(id);
		  params--;
		  this.wasParametersLoaded = true;
		  
		  JSONParser parser = new JSONParser();
		  int quantityClasses = 0;
		  
		  int z = 0;//spinnery
		  int j = 0;//combobox
		  int y = 0;//text fieldy
		  for(int i = params*5; i<params*5+5; i++){
			  try{
				  Object obj = parser.parse(distributionParameters.get(i));
				  JSONObject jsonObject = (JSONObject) obj;
				  
				  quantityClasses = Integer.parseInt(jsonObject.get("quantity").toString());
				  spinnersList.get(z).setValue(quantityClasses);
				  
				  comboBoxDistributionValues.get(j).setSelectedIndex(comboPosition(jsonObject.get("alfa-cb").toString()));
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("alfa-v1").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("alfa-v2").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("alfa-v3").toString());
				  y++;
				  j++;
				  
				  comboBoxDistributionValues.get(j).setSelectedIndex(comboPosition(jsonObject.get("beta-cb").toString()));
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("beta-v1").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("beta-v2").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("beta-v3").toString());
				  y++;
				  j++;
				  
				  comboBoxDistributionValues.get(j).setSelectedIndex(comboPosition(jsonObject.get("gamma-cb").toString()));
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("gamma-v1").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("gamma-v2").toString());
				  y++;
				  textFieldsDistributionValues.get(y).setText(jsonObject.get("gamma-v3").toString());
				  y++;
				  j++;
			  }catch (Exception e){
				  //exception
				  e.printStackTrace();
			  }
			  z++;
		  }
	  }else{
		  this.wasParametersLoaded = false;
		  int j= 0;
		  int y = 0;
		  for(int i = 0; i<5; i++)
		  {
			  spinnersList.get(i).setValue(0);
			  
			  comboBoxDistributionValues.get(j).setSelectedIndex(0);
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  j++;
			  
			  comboBoxDistributionValues.get(j).setSelectedIndex(0);
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  j++;
			  
			  comboBoxDistributionValues.get(j).setSelectedIndex(0);
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  textFieldsDistributionValues.get(y).setText("1.0");
			  y++;
			  j++;
		  }
	  }
  }
  
  public int comboPosition(String str){
	  int i = 0;
	  
	  switch (str){
		  case "Eksponencjalny": i = 0;
		  break;
		  case "Log-Normalny": i = 1;
		  break;
		  case "Jednostajny": i = 2;
		  break;
		  case "Uog�lnionyPareto": i = 3;
		  break;
		  default: i = 0;
		  break;		  
	  }
	  
	  return i;
  }
  
}
